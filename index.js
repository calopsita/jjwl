import { AppRegistry, Dimensions, View } from 'react-native'

import React, { Component } from 'react'

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux'
import { reduce } from './src/data/reducer'

import { persistStore, persistReducer } from 'redux-persist'
import { PersistGate } from 'redux-persist/lib/integration/react'
import storage from 'redux-persist/lib/storage'

import createSagaMiddleware from 'redux-saga'
import rootSaga from './src/data/sagas'

import EStyleSheet from 'react-native-extended-stylesheet'

import Entry from './src/components/entry'

import {name as appName} from './app.json'

const persistConfig = {
  key: 'root',
  storage: storage,
  whitelist: ['token']
}

const pReduce = persistReducer(persistConfig, reduce)

const sagaMiddleware = createSagaMiddleware()
const store = createStore(
  pReduce,
  applyMiddleware(sagaMiddleware)
)

const persistor = persistStore(store)
sagaMiddleware.run(rootSaga)


class App extends Component {
	constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Entry />
        </PersistGate>
      </Provider>
    )
	}
}

export default App

AppRegistry.registerComponent(appName, () => App)

let {height, width} = Dimensions.get('window')
const vUnit = height/1000
const hUnit = width/1000

EStyleSheet.build({
  $primaryColor100: 'hsla(190, 65%, 92%, 1)',
  $primaryColor200: 'hsla(192, 45%, 80%, 1)',
  $primaryColor300: 'hsla(194, 35%, 68%, 1)',
  $primaryColor400: 'hsla(196, 28%, 58%, 1)',
  $primaryColor500: 'hsla(198, 23%, 50%, 1)',
  $primaryColor600: 'hsla(198, 26%, 43%, 1)',
  $primaryColor700: 'hsla(200, 33%, 35%, 1)',
  $primaryColor800: 'hsla(202, 41%, 28%, 1)',
  $primaryColor900: 'hsla(204, 55%, 20%, 1)',
  $secondaryColor100: 'hsla(17, 93%, 93%, 1)',
  $secondaryColor200: 'hsla(16, 75%, 83%, 1)',
  $secondaryColor300: 'hsla(15, 62%, 75%, 1)',
  $secondaryColor400: 'hsla(14, 53%, 68%, 1)',
  $secondaryColor500: 'hsla(14, 47%, 61%, 1)',
  $secondaryColor600: 'hsla(14, 49%, 51%, 1)',
  $secondaryColor700: 'hsla(13, 55%, 41%, 1)',
  $secondaryColor800: 'hsla(12, 70%, 28%, 1)',
  $secondaryColor900: 'hsla(11, 95%, 14%, 1)',
  $tertiaryColor100: 'hsla(176, 45%, 90%, 1)',
  $tertiaryColor200: 'hsla(174, 32%, 73%, 1)',
  $tertiaryColor300: 'hsla(172, 24%, 65%, 1)',
  $tertiaryColor400: 'hsla(170, 18%, 58%, 1)',
  $tertiaryColor500: 'hsla(170, 14%, 50%, 1)',
  $tertiaryColor600: 'hsla(170, 18%, 40%, 1)',
  $tertiaryColor700: 'hsla(168, 24%, 30%, 1)',
  $tertiaryColor800: 'hsla(166, 32%, 20%, 1)',
  $tertiaryColor900: 'hsla(164, 45%, 10%, 1)',
  $grey000: 'hsla(213, 10%, 100%, 1)',
  $grey100: 'hsla(213, 10%, 98%, 1)',
  $grey200: 'hsla(213, 10%, 90%, 1)',
  $grey300: 'hsla(213, 10%, 84%, 1)',
  $grey400: 'hsla(213, 10%, 76%, 1)',
  $grey500: 'hsla(213, 10%, 59%, 1)',
  $grey600: 'hsla(213, 10%, 42%, 1)',
  $grey700: 'hsla(213, 10%, 16%, 1)',
  $fontSize1: 12,
  $fontSize2: 14,
  $fontSize3: 16,
  $fontSize4: 18,
  $fontSize5: 20,
  $fontSize6: 24,
  $fontSize7: 30,
  $fontSize8: 36,
  $fontSize9: 48,
  $fontSize10: 60,
  $fontSize11: 72,
  $size1: 1,
  $size2: 2,
  $size3: 3,
  $size4: 4,
  $size5: 8,
  $size6: 12,
  $size7: 16,
  $size8: 24,
  $size9: 32,
  $size9_5: 40,
  $size10: 48,
  $size10_5: 56,
  $size11: 64,
  $size12: 96,
  $size13: 128,
  $size14: 192,
  $size15: 256,
  $size16: 384,
  $size17: 512,
  $size18: 640,
  $size19: 768,
  $fontReg: 'Montserrat-Regular',
  $fontMed: 'Montserrat-Medium',
  $fontSemiBold: 'Montserrat-SemiBold',
  $fontBold: 'Montserrat-Bold',
  $fontExtraBold: 'Montserrat-ExtraBold',
  $hUnit: hUnit,
  $vUnit: vUnit,
  $rem: hUnit,
  $bgColor: '#fbfbfb',
})