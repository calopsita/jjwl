import React from 'react'
import { View, Text, Image } from 'react-native'

import moment from 'moment'

import EStyleSheet from 'react-native-extended-stylesheet'

function filterEvents(events, timeDirection, justRegistered) {
  let filteredEvents
  if (timeDirection == 'future') {
    filteredEvents = events.filter(e => moment(e.date, 'YYYY-MM-DD').isSameOrAfter(moment(), 'day'))
    if (justRegistered == true) {
      filteredEvents = (filteredEvents && filteredEvents.filter(e => e.registered == justRegistered))
    }
  } else if (timeDirection == 'past') {
    filteredEvents = events.filter(e => moment(e.date, 'YYYY-MM-DD').isBefore(moment(), 'day'))
    if (justRegistered == true) {
      filteredEvents = (filteredEvents && filteredEvents.filter(e => e.registered == justRegistered))
    }
  }
  return filteredEvents
}

function compareDates(a, b) {
  if (moment(a.date, 'YYYY-MM-DD').isSameOrAfter(moment(b.date, 'YYYY-MM-DD'))) {
    return -1
  }
  if (moment(a.date, 'YYYY-MM-DD').isBefore(moment(b.date, 'YYYY-MM-DD'))) {
    return 1
  }
  return 0
}

function compareDatesRev(b, a) {
  if (moment(a.date, 'YYYY-MM-DD').isSameOrAfter(moment(b.date, 'YYYY-MM-DD'))) {
    return -1
  }
  if (moment(a.date, 'YYYY-MM-DD').isBefore(moment(b.date, 'YYYY-MM-DD'))) {
    return 1
  }
  return 0
}

function groupByMonth(events) {
  let groupedEvents = []
  for (let i = 0; i < events.length; i++) {
    const dateBucket = moment(events[i].date, 'YYYY-MM-DD').format('YYYY-MM')
    let found = false
    for (let j = 0; j < groupedEvents.length; j++) {
      if (groupedEvents[j].date == dateBucket) {
        groupedEvents[j].events.push(events[i])
        found = true
        break
      }
    }
    if (!found) {
      groupedEvents.push({date: dateBucket, events: [events[i]]})
    }
  }
  return groupedEvents
}

export default function MyEventStream(props) {
  console.log('MyEventStream')
  const timeDirection = props.mode == 'Past Events' ? 'past' : 'future'
  const justRegistered = props.mode == 'Upcoming' ? false : true
  const filteredEvents = filterEvents(props.events, timeDirection, justRegistered)
  const filteredGroupedEvents = groupByMonth(filteredEvents)
  const filteredGroupedSortedEvents = timeDirection == 'past' ? filteredGroupedEvents.sort(compareDates) : filteredGroupedEvents.sort(compareDatesRev)
  const filteredGroupedSorted2Events = filteredGroupedSortedEvents.map(e => {
    return (
      {
        date: e.date, 
        events: timeDirection == 'past' ? e.events.sort(compareDates) : e.events.sort(compareDatesRev)
      }
    ) 
  })
  const eventGroups = filteredGroupedSorted2Events.map((g, i) => {
    const events = g.events.map((e, i) => {
      return (
        <View key={i} style={styles.eventContainer}>
          <View style={styles.eventImageItem}>
            <Image 
              source={{uri: e.img}}
              style={styles.eventImage}
            />
          </View>
          <View style={styles.eventInfoContainer}>
            <View style={styles.topInfo}>
              <View style={styles.infoTextContainer}>
                <Text style={styles.title}>{e.title}</Text>
                <Text style={styles.details}>{e.arena + ' | ' + moment(e.date, 'YYYY-MM-DD').format('MMM Do')}</Text>
              </View>
            </View>
            <View style={styles.bottomInfo}>
              <View style={styles.infoTextContainer}>
                <Text style={styles.subtitle}>{e.subtitle}</Text>
                <Text style={styles.details}>{e.ages}</Text>
                <Text style={styles.details}>{e.styles}</Text>
              </View>
            </View>
          </View>
        </View>
      )
    })
    return (
      <View key={i}>
        <View style={styles.monthContainer}>
          <Text style={styles.header}>
            {moment(g.date).format('MMMM')}
          </Text>
          {events}
        </View>
      </View>
    )
  })

  console.log(filteredGroupedSorted2Events)
  return (
    <View style={styles.streamContainer}>
      {eventGroups}
    </View>
	)
}

const styles = EStyleSheet.create({
  streamContainer: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: '15rem',
  },
  header: {
    fontFamily: '$fontBold',
    fontSize: '60rem',
    marginTop: '15 * $vUnit',
    marginBottom: '30 * $vUnit',
    marginLeft: '20rem',
  },
  monthContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  eventContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: '20 * $vUnit',
    backgroundColor: '$bgColor',
  },
  eventImageItem: {
    shadowOffset: { width: 0, height: 3 },
    shadowRadius: 10,
    shadowOpacity: .2,
    elevation: 5,
  },
  eventImage: {
    width: '400rem',
    height: '400rem',
    borderRadius: '30rem',
  },
  eventInfoContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
    height: '90%',
    shadowOffset: { width: 0, height: 3 },
    shadowRadius: 10,
    shadowOpacity: .2,
    elevation: 5,
  },
  topInfo: {
    height: '50%',
    width: '95%',
    backgroundColor: 'white',
    borderTopRightRadius: '30rem',
  },
  bottomInfo: {
    height: '50%',
    width: '95%',
    backgroundColor: '#f2f2f2',
    borderBottomRightRadius: '15rem',
  },
  infoTextContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: '20rem',
  },
  title: {
    fontFamily: '$fontMed',
    fontSize: '38rem',
    marginBottom: '10rem',
  },
  details: {
    fontFamily: '$fontReg',
    fontSize: '28rem',
  },
  subtitle: {
    fontFamily: '$fontMed',
    fontSize: '40rem',
    marginBottom: '10rem',
  },
})