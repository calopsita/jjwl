import React from 'react'
import { ScrollView, Image, ImageBackground, View, Text } from 'react-native'

import { connect } from 'react-redux'

import EStyleSheet from 'react-native-extended-stylesheet'

import MyHeader from '../utils/header'
import MySubheader from '../utils/subheader'
import MyEventStream from './event_stream'

class EventScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      subheaderIndex: 2
    }
    this.setSubheaderIndex = this.setSubheaderIndex.bind(this)
  }

  componentDidMount() {
    this.props.dispatch({
      type: 'RETRIEVE_EVENTS',
      payload: {
        token: this.props.token,
        userID: 0
      }
    })
  }

  setSubheaderIndex(i) {
    this.setState({subheaderIndex: i})
  }

  render() {
    console.log("EVENT PROPS")
    console.log(this.props)
    const subheaderOptions = ['Upcoming', 'Past Events', 'Registered']
    return (
      <ScrollView 
        stickyHeaderIndices={[0]}
        contentContainerStyle={styles.scrollContainer}
      >
        <MyHeader title='My Events'/>
        <View style={styles.subheaderContainer}>
          <MySubheader 
          	options={subheaderOptions}
          	index={this.state.subheaderIndex}
            setIndex={this.setSubheaderIndex}
          />
        </View>
        <View style={styles.eventsContainer}>
          <MyEventStream
            mode={subheaderOptions[this.state.subheaderIndex]}
            events={this.props.events}
          />
        </View>
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    events: state.events || [],
  }
}

export default connect(mapStateToProps)(EventScreen)


const styles = EStyleSheet.create({
  scrollContainer : {
    backgroundColor: '$bgColor',
  },
  subheaderContainer: {
    justifyContent: 'flex-start',
    alignSelf: 'center',
    width: '96%',
    height: '60 * $vUnit',
    marginTop: '0 * $vUnit',
    marginBottom: '0 * $vUnit',
    backgroundColor: '$bgColor',
  },
})