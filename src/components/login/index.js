import React, { Component } from 'react';
import { Keyboard, Dimensions, TouchableOpacity } from 'react-native';

import DismissKeyboard from 'dismissKeyboard'

import { connect } from 'react-redux'

import { Container, Button, Card, Form, Item, Input, Footer, Text, View } from 'native-base'
import { Grid, Col, Row } from 'react-native-easy-grid'

import KeyboardSpacer from 'react-native-keyboard-spacer'
import EStyleSheet from 'react-native-extended-stylesheet'

class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      keyboardShrinkHeight: null
    }

    this.handleFormChange = this.handleFormChange.bind(this)
    this.handleLoginSubmit = this.handleLoginSubmit.bind(this)
    this._keyboardDidShow = this._keyboardDidShow.bind(this)
    this._keyboardDidHide = this._keyboardDidHide.bind(this)
  }

  static navigationOptions = {
    header: null
  }

  componentDidMount() {
    console.log('componentDidMount')
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount() {
    console.log('componentWillUnmount')
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow(e) {
    console.log('_keyboardDidShow')
    let keyboardShrinkHeight = Dimensions.get('window').height - e.endCoordinates.height * 1.1
    this.setState({
      keyboardShrinkHeight: keyboardShrinkHeight,
    })
  }

  _keyboardDidHide() {
    console.log('_keyboardDidHide')
    this.setState({
      keyboardShrinkHeight: null,
    })
  }

  handleFormChange(k, v) {
    this.setState({[k]: v})
  }

  handleLoginSubmit() {
    console.log("Login pressed")
    this.props.dispatch({
      type: 'POST',
      task: 'login',
      payload: {
        username: this.state.username,
        password: this.state.password,
        deviceID: 1,
      },
      nextTypeSuccess: 'RETRIEVE_TOKEN_SUCCEEDED',
    })
  }

  render() {
    let footer
    let topRowStyles
    if (this.state.keyboardShrinkHeight == null) {
      footer = (
        <Footer>
          <View style={styles.footer}>
              <Button full style={styles.lostPasswordButton}><Text style={styles.lostPasswordButtonText}>Lost Password</Text></Button>
              <Button full style={styles.createAccountButton}><Text style={styles.createAccountButtonText}>Create Account</Text></Button>
          </View>
        </Footer>
      )
      topRowStyles = Object.assign({}, styles.topRow)
    } else {
      footer = null
      topRowStyles = Object.assign({}, styles.topRow, {height: this.state.keyboardShrinkHeight - styles.loginRow.height})
    }

    return (
      <Container>
        <View style={styles.loginContainer}>
          <TouchableOpacity onPress={Keyboard.dismiss} style={topRowStyles} />
          <View style={styles.loginRow}>
            <View style={styles.loginCol} >
              <Card style={styles.loginCard}>
                <Form>
                  <Item>
                    <Input 
                      placeholder='Username'
                      placeholderTextColor='#aeaeae'
                      autoCapitalize='none'
                      onChangeText={(v) => this.handleFormChange('username', v)}
                      value={this.state.username}
                      style={styles.loginInput}
                      autoCorrect={false}
                    />
                  </Item>
                  <Item last>
                    <Input 
                      placeholder="Password" 
                      placeholderTextColor="#aeaeae"
                      autoCapitalize='none'
                      onChangeText={(v) => this.handleFormChange('password', v)}
                      value={this.state.password}
                      secureTextEntry 
                      style={styles.loginInput}
                      />
                  </Item>
                </Form>
                <Button dark block onPress={this.handleLoginSubmit} style={styles.loginSubmitButton}>
                  <Text style={styles.loginSubmitButtonText}>Sign In</Text>
                </Button>
              </Card>
            </View>
          </View>
          <View style={styles.bottomRow}>
            <View style={styles.bottomCol}>
            </View>
          </View>
        </View>
        {footer}
      </Container>
    );
  }
}

export default connect()(LoginScreen)

const styles = EStyleSheet.create({
  loginContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  topRow: {
    backgroundColor: '$bgColor',
    height:'350 * $vUnit',
    width: '100%',
  },
  topCol: {
  },
  loginRow: {
    backgroundColor:'$bgColor',
    height: '300 * $vUnit',
    width: '100%'
  },
  loginCol: {
  },
  bottomRow: {
    backgroundColor: '$bgColor',
    height: '350 * $vUnit',
    width: '100%',
  },
  bottomCol: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  loginCard: {
    marginTop: 0,
    marginBottom: 0,
    marginLeft: 0,
    marginRight: 0,
    paddingTop: 0,
    paddingBottom: 0,
    backgroundColor: '$bgColor',
    borderColor: '$bgColor',
    shadowOffset: { width: 0, height: 3 },
    shadowRadius: 10,
    shadowOpacity: .2,
    elevation: 5,
  },
  loginInput: {
    fontFamily: '$fontReg',
  },
  loginSubmitButton: {
    marginLeft: '20rem',
    marginRight: '20rem',
    marginTop: '50rem',
    marginBottom: '50rem',
  },
  loginSubmitButtonText: {
    fontSize: '50rem',
    paddingBottom: '1rem',
    fontFamily: '$fontReg',
  },
  footer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  lostPasswordButton: {
    backgroundColor: '$bgColor',
    borderColor: '#6b6b6b',
    borderWidth: 1,
    height: '100%',
    width: '50%',
  },
  lostPasswordButtonText: {
    color: '#a1a1a1',
    fontFamily: '$fontReg',
  },
  createAccountButton: {
    backgroundColor: '#a1a1a1',
    borderColor: '#a1a1a1',
    borderWidth: 1,
    height: '100%',
    width: '50%',
  },
  createAccountButtonText: {
    color: '$bgColor',
    fontFamily: '$fontReg',
  }
})
