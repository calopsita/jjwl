import React from 'react'
import { View, Text, TouchableHighlight } from 'react-native'

import LinearGradient from 'react-native-linear-gradient'

import EStyleSheet from 'react-native-extended-stylesheet'

const FilterRadio = (props) => {
  const allOptions = props.options.map(i => {
    const handleUpdateGroup = () => props.updateGroup(props.cat, i.id)
    if (i.active) {
      return (
        <View key={i.id} style={styles.option}>
          <LinearGradient
            style={styles.insetShadow}
            colors={['#FFFFFF', '#D1D1D1', '#000000']}
            locations={[0, .4, .95]}
            start={{x: .3, y: .9}}
            end={{x: .3, y: .1}}
          >
            <TouchableHighlight 
              underlayColor='#ffffff'
              onPress={handleUpdateGroup}
              style={[styles.radioBase, styles.radioOn]}>
              <View style={styles.optionTextContainer}>
                <Text style={styles.optionText}>
                  {i.n}
                </Text>
              </View>
            </TouchableHighlight>
          </LinearGradient>
        </View>
      )
    } else {
      return (
        <View key={i.id} style={styles.option}>
          <TouchableHighlight 
            underlayColor='#000000'
            onPress={handleUpdateGroup}
            style={[styles.radioBase, styles.radioOff]}>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>
                {i.n}
              </Text>
            </View>
          </TouchableHighlight>
        </View>
      )
    }
  })
  return (
    <View 
      style={EStyleSheet.child(styles, 'cat', props.index, props.count)}>
      <View style={styles.controlHeader}>
        <Text style={styles.controlHeaderText}>{props.title}</Text>
      </View>
      <View style={styles.optionContainer}>
        {allOptions}
      </View>
    </View>
  ) 
}

export default FilterRadio


const styles = EStyleSheet.create({
  cat: {
    marginBottom: '$size6',
    backgroundColor: '$grey100',
  },
  'cat:first-child': {
    marginTop: '$size5',
  },
  controlHeader: {
    marginTop: '$size5',
    marginBottom: '$size6',
    marginLeft: '$size7',
  },
  controlHeaderText: {
    fontFamily: '$fontMed',
  },
  optionContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: '$size10',
  },
  option: {
    marginRight: '$size10',
  },
  radioBase: {
    borderRadius: '$size13 * .2',
    borderWidth: '$size1',
  },
  radioOn: {
    height: '$size10 - $size1',
    width: '$size13 - $size1',
    borderColor: '#00000000',
    backgroundColor: '$grey200',
  },
  radioOff: {
    height: '$size10',
    width: '$size13',
    borderColor: '$grey700',
    backgroundColor: '$grey000',
  },
  insetShadow: {
    flex: 1,
    alignItems: 'center', 
    justifyContent: 'center',
    height: '$size10',
    width: '$size13',
    borderRadius: '$size13 * .2',
  },
  optionTextContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  optionText: {
    fontFamily: '$fontReg',
  },
})