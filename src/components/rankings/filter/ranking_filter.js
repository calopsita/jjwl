import React, { useRef, useState, useEffect } from 'react'

import { View, Text, ScrollView, TouchableOpacity, Animated, Easing } from 'react-native'

import NativeMethodsMixin from 'NativeMethodsMixin'

import Icon from 'react-native-vector-icons/Feather'

import EStyleSheet from 'react-native-extended-stylesheet'

import MyHeader from '../../utils/header'
import FilterHeader from '../filter_header/filter_header'
import FilterRadio from './radio'
import HeaderChip from './header_chip'
import FilterMultiSelect from './filter_multiselect'
import FilterBeltSelect from './belt_select'
import FilterFooter from './filter_footer'

const RankingFilter = (props) => {
  const scrollContainerRef = useRef(null)
  const [multiExpanded, setMultiExpanded] = useState({ current: null, last: null })
  const [multiExpandedLayout, setMultiExpandedLayout] = useState({ current: null, last: null })
  const [controlLayout, setControlLayout] = useState(null)

  useEffect(() => { alignMultiFilterComponent() }, [multiExpandedLayout])

  const handleSetMultiExpandedLayout = (action, layout) => {
    if (action == 'expand') {
      setMultiExpandedLayout({ current: layout, last: setMultiExpandedLayout.current })
    } else if (action == 'collapse') {
      setMultiExpandedLayout({ current: null, last: layout })
    }
  }
  const handleSetControlLayout = (e) => setControlLayout(e.nativeEvent.layout)

  const alignMultiFilterComponent = () => {
    if (multiExpandedLayout.current != null || multiExpandedLayout.last != null) {
      NativeMethodsMixin.measure.call(scrollContainerRef.current, (x, y, width, height, px, py) => {
        const layout = multiExpandedLayout.current || multiExpandedLayout.last
        const scrollBottom = y + height
        const filterTop = y + controlLayout.y + layout.y
        const filterBottom = filterTop + layout.height
        const overflow = filterBottom - scrollBottom
        if (multiExpandedLayout.current != null) {
          if (overflow > 0) {
            const fromTop = filterTop - overflow
            if (fromTop < 0) {
              const filterGap = filterTop - y
              scrollContainerRef.current.scrollTo({x: 0, y: filterGap})
            } else {
              scrollContainerRef.current.scrollTo({x: 0, y: overflow})
            }
          }
        } else if (multiExpandedLayout.last != null) {
          scrollContainerRef.current.scrollTo({x: 0, y: 0})
        }
      })
    }
  }

  return (
    <View style={styles.parent}>
      <MyHeader title='Individual Rankings'/>
      <ScrollView
        ref={scrollContainerRef}
        contentContainerStyle={styles.scrollContainer}
      >
        <FilterHeader isFilterOpen={true} handleSelect={props.handleSelect}/>
        <View 
          onLayout={handleSetControlLayout} 
          style={styles.control}>
          <View style={styles.catContainer}>
            <FilterRadio
              cat={'ageGroup'}
              title={'age group'}
              options={props.ageGroupOptions}
              updateGroup={props.updateGroup}
              index={0}
              count={2}
            />
            <FilterRadio
              cat={'gender'}
              title={'gender'}
              options={props.genderOptions}
              updateGroup={props.updateGroup}
              index={1}
              count={2}
            />
            <FilterMultiSelect
              cat={'a'}
              title={'age'}
              multiExpanded={multiExpanded}
              options={props.ageOptions}
              setMultiExpanded={setMultiExpanded}
              handleSetMultiExpandedLayout={handleSetMultiExpandedLayout}
              updateMultiCheck={props.updateMultiCheck}
            />
            <FilterMultiSelect
              cat={'w'}
              title={'weight'}
              multiExpanded={multiExpanded}
              options={props.weightOptions}
              setMultiExpanded={setMultiExpanded}
              handleSetMultiExpandedLayout={handleSetMultiExpandedLayout}
              updateMultiCheck={props.updateMultiCheck}
            />
            <FilterBeltSelect
              cat={'b'}
              title={'belt'}
              options={props.beltOptions}
              updateMultiCheck={props.updateMultiCheck}
            />
            <FilterMultiSelect
              cat={'season'}
              title={'season'}
              multiExpanded={multiExpanded}
              options={props.seasonOptions}
              setMultiExpanded={setMultiExpanded}
              handleSetMultiExpandedLayout={handleSetMultiExpandedLayout}
              updateMultiCheck={props.updateMultiCheck}
            />
          </View>
        </View>
      </ScrollView>
      <FilterFooter />
    </View>
	)
}

export default RankingFilter

const styles = EStyleSheet.create({
  parent: {
    flex: 1,
    backgroundColor: '$bgColor',
  },
  scrollContainer: {
    flexGrow: 1,
  },
  filterContainer: {
    flex: 1,
    flexDirection: 'row',
    width: '95%',
    height: '130rem',
    alignSelf: 'center',
  },
  iconContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: '10rem',
  },
  filterIcon: {
    color: 'black',
    fontSize: '90rem',
    alignSelf: 'center',
    marginLeft: '15rem',
    marginRight: '15rem',
  },
  summary: {
    width: '80%',
  },
  summaryContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: '110rem',
    marginLeft: '20rem',
  },
  bottomBorder : {
    borderBottomWidth: 2,
    borderBottomColor: '#ededed',
    width: '100%',
    alignSelf: 'center',
  },
  control: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#F9F9F9'
  },
  controlHeader: {
    marginTop: '20rem',
    marginBottom: '30rem',
    marginLeft: '50rem',
  },
  controlHeaderText: {
    fontFamily: '$fontMed',
  },
  catContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  cat: {
    marginTop: '30rem',
  },
})