import React from 'react'
import { View, Text } from 'react-native'

import EStyleSheet from 'react-native-extended-stylesheet'

const HeaderChip = (props) => {
  const headerChip = props.active.length == 0 ? null : (
    <View style={styles.group}>
      <View><Text style={styles.groupHeaderText}>{props.title}</Text></View>
      <View style={styles.groupContent}>
        {props.active.map((i, idx) => {
          const txt = idx == 0 ? i.n : ', ' + i.n
          return <View key={i.id+'|'+i.n}><Text style={styles.groupText}>{txt}</Text></View>
        })}
      </View>
    </View>
  )
  return headerChip
}

export default HeaderChip

const styles = EStyleSheet.create({
  group: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    paddingLeft: '30rem',
    paddingRight: '30rem',
    paddingTop: '10rem',
    marginTop: '10rem',
    marginRight: '10rem',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: 'black',
  },
  groupHeaderText: {
    fontFamily: '$fontReg',
    fontSize: 10,
  },
  groupContent: {
    flex: 1,
    flexDirection: 'row',
  },
  groupText: {
    fontFamily: '$fontReg',
  },
})