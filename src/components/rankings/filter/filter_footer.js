import React from 'react'
import { View, Text } from 'react-native'

import EStyleSheet from 'react-native-extended-stylesheet'

const FilterFooter = (props) => {
  return (
    <View style={styles.footer}>
      <View style={styles.buttonContainer}>
        <View>
          <Text style={styles.buttonText}>UPDATE FILTER</Text>
        </View>
      </View>
    </View>
  )
}

export default FilterFooter

const styles = EStyleSheet.create({
  footer: {
    height: '200rem',
    backgroundColor: 'black',
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '25rem',
    backgroundColor: 'black',
  },
  buttonText: {
    fontFamily: '$fontMed',
    fontSize: 24,
    color: 'white',
  },
})