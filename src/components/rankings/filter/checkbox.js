import React from 'react'

import { Animated, Easing, TouchableOpacity } from 'react-native'

import { connect } from 'react-redux'

import EStyleSheet from 'react-native-extended-stylesheet'


class MyCheckbox extends React.Component {
  constructor(props) {
    super(props)
    
    const boxRotateRange = ['0deg', '45deg']
    const activeColorRange = ['#000000', 'red']
    const inactiveColorRange = ['#000000', '#00000000']
    const boxWidthRange = [20 * props.scale, 10 * props.scale]
    const boxHeightRange = [20 * props.scale, 23 * props.scale]
    const boxTranslateYRange = [0, -8 * props.scale]
    const boxTranslateXRange = [0, 0]
    const rangeIdx = props.active ? 1 : 0

    this.state = {
      boxRotate: boxRotateRange[rangeIdx],
      boxRotateRange: boxRotateRange,
      activeColor: activeColorRange[rangeIdx],
      activeColorRange: activeColorRange,
      inactiveColor: inactiveColorRange[rangeIdx],
      inactiveColorRange: inactiveColorRange,
      boxWidth: boxWidthRange[rangeIdx],
      boxWidthRange: boxWidthRange,
      boxHeight: boxHeightRange[rangeIdx],
      boxHeightRange: boxHeightRange,
      boxTranslateY: boxTranslateYRange[rangeIdx],
      boxTranslateYRange: boxTranslateYRange,
      boxTranslateX: boxTranslateXRange[rangeIdx],
      boxTranslateXRange: boxTranslateXRange,
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.active != this.props.active) {
      let fromIdx
      let toIdx
      if (this.props.active) {
        fromIdx = 0
        toIdx = 1
      } else {
        fromIdx = 1
        toIdx = 0
      }
      let spin = new Animated.Value(0)
      Animated.timing(
          spin,
        {
          toValue: 1,
          duration: 200,
          easing: Easing.linear
        }
      ).start()
      const boxRotateNext = spin.interpolate({
        inputRange: [0, 1],
        outputRange: [this.state.boxRotateRange[fromIdx], this.state.boxRotateRange[toIdx]],
      })
      const activeColorNext = spin.interpolate({
        inputRange: [0, 1],
        outputRange: [this.state.activeColorRange[fromIdx], this.state.activeColorRange[toIdx]],
      })
      const inactiveColorNext = spin.interpolate({
        inputRange: [0, 1],
        outputRange: [this.state.inactiveColorRange[fromIdx], this.state.inactiveColorRange[toIdx]],
      })
      const boxWidthNext = spin.interpolate({
        inputRange: [0, 1],
        outputRange: [this.state.boxWidthRange[fromIdx], this.state.boxWidthRange[toIdx]],
      })
      const boxHeightNext = spin.interpolate({
        inputRange: [0, 1],
        outputRange: [this.state.boxHeightRange[fromIdx], this.state.boxHeightRange[toIdx]],
      })
      const boxTranslateYNext = spin.interpolate({
        inputRange: [0, 1],
        outputRange: [this.state.boxTranslateYRange[fromIdx], this.state.boxTranslateYRange[toIdx]],
      })
      const boxTranslateXNext = spin.interpolate({
        inputRange: [0, 1],
        outputRange: [this.state.boxTranslateXRange[fromIdx], this.state.boxTranslateXRange[toIdx]],
      })
      this.setState({
        boxRotate: boxRotateNext,
        activeColor: activeColorNext,
        inactiveColor: inactiveColorNext,
        boxWidth: boxWidthNext,
        boxHeight: boxHeightNext,
        boxTranslateY: boxTranslateYNext,
        boxTranslateX: boxTranslateXNext,
      })
    }
  }

  render() {
    return (
      <TouchableOpacity style={styles.touch} onPress={() => this.props.toggle()}>
        <Animated.View 
          style={
            [
              styles.box, 
              {transform: [
                {rotate: this.state.boxRotate},
                {translateY: this.state.boxTranslateY},
                {translateX: this.state.boxTranslateX},
              ]},
              {borderRightColor: this.state.activeColor, borderBottomColor: this.state.activeColor},
              {borderLeftColor: this.state.inactiveColor, borderTopColor: this.state.inactiveColor},
              {width: this.state.boxWidth, height: this.state.boxHeight},
            ]
          }
        />
      </TouchableOpacity>
    )
  }
}

export default MyCheckbox

const styles = EStyleSheet.create({
  touch: {
    width: '$size9',
    height: '$size8',
  },
  box: {
    borderRadius: '$size2',
    borderWidth: '$size2',
  }
})