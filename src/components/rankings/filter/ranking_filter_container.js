import React from 'react'

import { connect } from 'react-redux'

import getCatOptions from '../../../data/selectors/cat_options'

import RankingFilter from './ranking_filter'

class RankingFilterContainer extends React.Component {
  constructor(props) {
    super(props)

    this.updateMultiCheck = this.updateMultiCheck.bind(this)
    this.updateGroup = this.updateGroup.bind(this)
  }

  updateMultiCheck(cat, id) {
    this.props.dispatch({
      type: 'UPDATE_RANK_FILTER_MULTICHECK',
      cat: cat,
      id: id,
    })
  }

  updateGroup(cat, id) {
    let nextGroupId
    const subGroups = Object.assign({ ageGroup: this.props.ageGroupPending, gender: this.props.genderPending }, { [cat]: id })
    for (let [k, v] of Object.entries(this.props.groups)) {
      if (v.ageGroup.id == subGroups.ageGroup && v.gender.id == subGroups.gender) {
        nextGroupId = k
        break
      }
    }
    this.props.dispatch({
      type: 'UPDATE_RANK_FILTER_GROUP',
      id: nextGroupId
    })
  }

  render() {
    return (
      <RankingFilter
        handleSelect={this.props.handleSelect}
        ageGroupOptions={this.props.ageGroupOptions}
        genderOptions={this.props.genderOptions}
        seasonOptions={this.props.seasonOptions}
        beltOptions={this.props.beltOptions}
        ageOptions={this.props.ageOptions}
        weightOptions={this.props.weightOptions}
        updateGroup={this.updateGroup}
        updateMultiCheck={this.updateMultiCheck}
        setControlLayout={this.props.setControlLayout}
        setFilterComponentLayout={this.props.setFilterComponentLayout}
        catExpandToggle={this.props.catExpandToggle}
      />

    )
  }
}

const makeMapStateToProps = () => {
  const prepCatOptionsOne = (catOptions, active) => Object.values(catOptions).map(i => ({ ...i, active: active == i.id}))
  const prepCatOptionsMulti = (catOptions, active) => Object.values(catOptions).map(i => ({ ...i, active: active.has(i.id)}))
  const mapStateToProps = (state) => {
    const categoryData = state.rankFilterCats.categoryData
    const pending = state.rankFilter.pending
    const groups = categoryData.g
    const groupPending = groups[pending.g]
    const ageGroupPending = groupPending.ageGroup.id
    const genderPending = groupPending.gender.id
    const catOptions = getCatOptions(state)
    return {
      groups: groups,
      ageGroupPending: ageGroupPending,
      genderPending: genderPending,
      ageGroupOptions: prepCatOptionsOne(categoryData.ageGroup, ageGroupPending),
      genderOptions: prepCatOptionsOne(categoryData.gender, genderPending),
      seasonOptions: prepCatOptionsMulti(categoryData.season, pending.season),
      ageOptions: prepCatOptionsMulti(catOptions.a, pending.a),
      beltOptions: prepCatOptionsMulti(catOptions.b, pending.b),
      weightOptions: prepCatOptionsMulti(catOptions.w, pending.w),
    }
  }
  return mapStateToProps
}

export default connect(makeMapStateToProps)(RankingFilterContainer)