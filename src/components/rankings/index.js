import React from 'react'

import RankingsContainer from './rankings_container'

function RankingsScreen(props) {
	return (
    <RankingsContainer />
  )
}

export default RankingsScreen