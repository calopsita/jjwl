import React, { useState } from 'react'
import { View } from 'react-native'

import EStyleSheet from 'react-native-extended-stylesheet'

import RankingResultsFightersOne from './ranking_results_fighters_one'


function RankingResultsFighters(props) {
  const [expandedFighter, setExpandedFighter] = useState(null)
  const makeSetHandleExpandedFighterToggle = (id) => () => (setExpandedFighter(id))
  const handleSetExpandedFighterUntoggle = () => (setExpandedFighter(null))
  let lastPoints
  let lastRank
  const rankingEntries = props.rankings.map((i, idx) => {
    const isExpanded = expandedFighter == idx
    const handleSetExpandedFighter = isExpanded ? handleSetExpandedFighterUntoggle : makeSetHandleExpandedFighterToggle(idx)
    const rank = lastPoints && lastPoints == i.rankPoints ? lastRank : idx + 1
    lastPoints = i.rankPoints
    lastRank = rank
    return (
      <RankingResultsFightersOne
        key={idx}
        id={idx}
        rankEntry={i}
        rank={rank}
        isExpanded={isExpanded}
        handleSetExpandedFighter={handleSetExpandedFighter}
      />
    )
  })
  return (
    <View style={styles.parent}>
      {rankingEntries}
    </View>
	)
}

export default RankingResultsFighters

const styles = EStyleSheet.create({
  parent: {
    flex: 1,
    flexDirection: 'column',
    marginTop: '20rem',
    backgroundColor: 'white',
  },
})