import React from 'react'

import { connect } from 'react-redux'

import utils from './utils'
import RankingResultsFighters from './ranking_results_fighters'


class RankingResultsFightersContainer extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.dispatch({
      type: 'RETRIEVE_RANKINGS',
      payload: {}
    })
  }

  render() {
    return (
      <RankingResultsFighters 
        rankings={this.props.rankings}
      />
    )
  }
}

function mapStateToProps(state, ownProps) {
  let rankings = state.rankings || []
  rankings = rankings.map(i => {
    if (i.timelines != undefined) {
      i.rankPoints = utils.getRankPoints(i.timelines['2018'])
      i.medalCount = utils.getMedalCount(i.timelines['2018'])
    }
    return i
  })
  rankings = utils.sort(rankings)
  
  return {
    rankings: rankings,
  }
}

export default connect(mapStateToProps)(RankingResultsFightersContainer)