import React, { useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

import { Thumbnail } from 'native-base'

import EStyleSheet from 'react-native-extended-stylesheet'

import FighterSeasons from './season/fighter_seasons'

function FighterAvatar(props) {
  return (
    <View style={styles.avatarItem}>
      <Thumbnail source={{uri: props.img}} />
    </View>
  )
}

function Blurb(props) {
  return (
    <View style={styles.blurb}>
      <View style={styles.blurbContainer}>
        <View style={styles.blurbItem}><Text style={styles.blurbHeader}>{props.rank + ' - ' + props.name}</Text></View>
        <View><Text style={styles.blurbSubheader}>{props.gymName}</Text></View>
      </View>
    </View>
  )
}

function Trophies(props) {
  const trophyClasses = ['trophyGold', 'trophySilver', 'trophyBronze']
  const trophies = trophyClasses.map((i, idx) => (
    <View key={idx} style={styles.oneTrophyContainer}>
      <View><Icon name='trophy' style={styles[trophyClasses[idx]]}/></View> 
      <View><Text style={styles.trophyText}>{props.medalCount[(idx+1).toString()]}</Text></View>
    </View>
  ))
  return (
    <View style={styles.trophies}>
      <View style={styles.trophyContainer}>
        {trophies}
      </View>
    </View>
  )
}

function Points(props) {
  return (
    <View style={styles.points}>
      <View style={styles.pointsContainer}>
        <View><Text style={styles.pointsHeader}>Points</Text></View>
        <View><Text style={styles.pointsText}>{props.rankPoints}</Text></View>
      </View>
    </View>
  )
}


function RankingResultsFightersOne(props) {
  const [expandedSeason, setExpandedSeason] = useState(null)
  const handleSetExpandedFighterOne = () => { props.handleSetExpandedFighter(); setExpandedSeason(null) }
  const summary = (
    <View>
      <TouchableOpacity onPress={handleSetExpandedFighterOne}>
        <View style={props.isExpanded ? styles.summaryParentContainerExpanded : styles.summaryParentContainer}>
          <FighterAvatar img={props.rankEntry.img} />
          <View style={styles.info}>
            <View style={props.isExpanded ? styles.infoContainerExpanded : styles.infoContainer}>
              <Blurb rank={props.rank} name={props.rankEntry.name} gymName={props.rankEntry.gymName} />
              <View style={styles.stats}>
                <View style={styles.statsContainer}>
                  <Trophies medalCount={props.rankEntry.medalCount} />
                  <Points rankPoints={props.rankEntry.rankPoints} />
                </View>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  )
  const seasonSummaries = props.isExpanded ? (
    <FighterSeasons
      timelines={props.rankEntry.timelines}
      expandedSeason={expandedSeason}
      setExpandedSeason={setExpandedSeason}
    />
  ) : null

  return (
    <View style={styles.parentContainer}>
      {summary}
      {seasonSummaries}
    </View>
  )
}

export default RankingResultsFightersOne

const baseStyles = {
  summaryParentContainerBase: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    backgroundColor: 'white',
    paddingTop: '10rem',
    paddingBottom: '10rem',
    elevation: 5,
    shadowOffset: { width: 0, height: 3 },
    shadowRadius: 0,
    shadowOpacity: 0,
  },
  infoContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '100%',
    borderBottomWidth: 1,
    borderBottomColor: '#ededed',
  }
}

const styles = EStyleSheet.create({
  parentContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  summaryParentContainer: baseStyles.summaryParentContainerBase,
  summaryParentContainerExpanded: {
    ...baseStyles.summaryParentContainerBase,
    shadowRadius: 1,
    shadowOpacity: .3,
  },
  avatarItem: {
    width: '20%',
    paddingLeft: '20rem',
    paddingRight: '20rem',
  },
  info: {
    width: '80%',
  },
  infoContainer: baseStyles.infoContainer,
  infoContainerExpanded: {
    ...baseStyles.infoContainer,
    borderBottomWidth: null,
    borderBottomColor: null,
  },
  blurbContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  blurb: {
    width: '70%',
    paddingBottom: '5 * $vUnit',
  },
  blurbItem: {
    marginBottom: '3 * $vUnit',
  },
  blurbHeader: {
    fontFamily: '$fontReg',
    fontSize: '40rem',
    color: '#8b8b8b',
  },
  blurbSubheader: {
    fontFamily: '$fontReg',
    fontSize: '30rem',
    color: '#c7c7c7',
  },
  stats: {
    width: '30%',
    paddingLeft: '10rem',
    paddingRight: '20rem',
    paddingBottom: '5 * $vUnit',
  },
  statsContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
  },
  trophies: {
    width: '40%',
    paddingRight: '10rem',
  },
  trophyContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  oneTrophyContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  trophyGold: {
    fontSize: 16,
    color: '#bf953f',
  },
  trophySilver: {
    fontSize: 16,
    color: '#c0c0c0',
  },
  trophyBronze: {
    fontSize: 16,
    color: '#b87333',
  },
  trophyText: {
    fontFamily: '$fontReg',
    fontSize: '25rem',
    marginLeft: '10rem',
  },
  points: {
    width: '60%',
  },
  pointsContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pointsHeader: {
    fontFamily: '$fontReg',
    fontSize: '25rem',
  },
  pointsText: {
    fontFamily: '$fontSemiBold',
    fontSize: '77rem',
    marginTop: '-3 * $vUnit',
  }
})