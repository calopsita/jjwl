import React from 'react'
import { View } from 'react-native'

import EStyleSheet from 'react-native-extended-stylesheet'

import FighterSeasonOne from './fighter_season_one'

function FighterSeasons(props) {
  const makeSetHandleExpandedSeasonToggle = (season) => () => props.setExpandedSeason(season)
  const handleSetExpandedSeasonUntoggle = () => props.setExpandedSeason(null)
  const sortedTimelines = Object.keys(props.timelines).sort((a, b) => parseInt(b) - parseInt(a))
  const seasons = sortedTimelines.map((i, idx) => {
    const isExpanded = i == props.expandedSeason
    const handleSetExpandedSeason = isExpanded ? handleSetExpandedSeasonUntoggle : makeSetHandleExpandedSeasonToggle(i)
    return (
      <FighterSeasonOne
        key={i}
        year={i}
        isExpanded={isExpanded}
        tournaments={props.timelines[i]}
        handleSetExpandedSeason={handleSetExpandedSeason}
      />
    )
  })

  return (
    <View style={styles.parent}>
      <View style={styles.parentContainer}>
        {seasons}
      </View>
    </View>
  )
}

export default FighterSeasons

const styles = EStyleSheet.create({
  parent: {
  },
  parentContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
})