import React from 'react'
import { View, Text, TouchableOpacity} from 'react-native'

import EStyleSheet from 'react-native-extended-stylesheet'

import Icon from 'react-native-vector-icons/FontAwesome'

import utils from '../utils'
import FighterTournaments from './fighter_tournaments'

function Season(props) {
  return (
    <View style={styles.year}>
      <View style={styles.yearContainer}>
        <View>
          <Text style={styles.yearText}>{props.year}</Text>
        </View>
      </View>
    </View>
  )
}

function Trophies(props) {
  const medalCount = utils.getMedalCount(props.tournaments)
  const trophyClasses = ['trophyGold', 'trophySilver', 'trophyBronze']
  const trophies = trophyClasses.map((i, idx) => (
    <View key={idx}>
      <View style={styles.oneTrophyContainer}>
        <View><Icon name='trophy' style={styles[trophyClasses[idx]]}/></View> 
        <View><Text style={styles.trophyText}>{medalCount[(idx+1).toString()]}</Text></View>
      </View>
    </View>
  ))
  return (
    <View style={styles.trophies}>
      <View style={styles.trophiesContainer}>
        {trophies}
      </View>
    </View>
  )
}

function Record(props) {
  const recordParams = [['Subs', utils.getSubCount(props.tournaments)], ['Wins', utils.getWinCount(props.tournaments)], ['Points', utils.getRankPoints(props.tournaments)]]
  const record = recordParams.map((i, idx) => (
    <View key={idx}>
      <View style={styles.oneRecordContainer}>
        <View><Text style={styles.recordHeaderText}>{i[0]}</Text></View>
        <View><Text style={styles.recordText}>{i[1]}</Text></View>
      </View>
    </View>
  ))   
  return (
    <View style={styles.record}>
      <View style={styles.recordContainer}>
        {record}
      </View>
    </View>
  )
}


function FighterSeasonOne(props) {
  console.log('***FighterSeasonOne')
  console.log(props)
  const tournaments = props.isExpanded ? <FighterTournaments tournaments={props.tournaments} /> : null
  console.log(tournaments)
  return (
    <View style={styles.parent}>
      <View style={styles.parentContainer}>
        <TouchableOpacity onPress={props.handleSetExpandedSeason}>
          <View style={styles.summary}>
            <View style={styles.summaryContainer}>
              <Season year={props.year} />
              <Trophies tournaments={props.tournaments} />
              <Record tournaments={props.tournaments} />
            </View>
          </View>
        </TouchableOpacity>
      </View>
      <View>
        {tournaments}
      </View>
    </View>
  )
}

export default FighterSeasonOne



const styles = EStyleSheet.create({
  parent: {
    width: '100%',
  },
  parentContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  summary: {
    width: '100%',
  },
  summaryContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    marginTop: '12rem',
    borderBottomWidth: 2,
    borderBottomColor: '#ededed',
    paddingTop: '8 * $vUnit',
    paddingBottom: '8 * $vUnit',
  },
  year: {
    width: '20%',
  },
  yearContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  yearText: {
    fontFamily: '$fontMed',
    fontSize: '50rem',
  },
  trophies: {
    width: '20%',
    paddingTop: '12rem',
    marginRight: '30rem',
  },
  trophiesContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  oneTrophyContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  trophyGold: {
    fontSize: 16,
    color: '#bf953f',
  },
  trophySilver: {
    fontSize: 16,
    color: '#c0c0c0',
  },
  trophyBronze: {
    fontSize: 16,
    color: '#b87333',
  },
  trophyText: {
    fontFamily: '$fontReg',
    fontSize: '25rem',
  },
  record: {
    width: '40%',
    paddingRight: '43rem',
  },
  recordContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  oneRecordContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  recordHeaderText: {
    fontFamily: '$fontReg',
    fontSize: '25rem',
  },
  recordText: {
    fontFamily: '$fontSemiBold',
    fontSize: '77rem',
    marginTop: '-3 * $vUnit',
  }
})