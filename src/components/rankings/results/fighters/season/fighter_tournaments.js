import React from 'react'
import { View, Text, Image } from 'react-native'

import { Icon } from 'native-base'

import EStyleSheet from 'react-native-extended-stylesheet'

function Header(props) {
  const medal = props.tournament.medal.rank ? (
    <View style={styles.medal}>
      <Image
        source={{uri: props.tournament.medal.img}}
        style={styles.medalPic}
      />
    </View>
  ) : null
  
  return (
    <View style={styles.header}>
      <View style={styles.headerContainer}>
        <View style={styles.headerTitle}>
          <Text style={styles.headerTitleText}>{props.tournament.name}</Text>
        </View>
        {medal}
      </View>
    </View>
  )
}

function Opponents(props) {
  const opponents = props.opponents.map((i, idx) => (
    <View key={idx} style={styles.oneResultBody}>
      <Text style={styles.opponentText}>{i}</Text>
    </View>
  ))

  return (
    <View style={styles.opponents}>
      <View style={styles.opponentContainer}>
        <View><Text style={styles.resultHeaderText}>Opponent</Text></View>
        {opponents}
      </View>
    </View>
  )
}

function Outcomes(props) {
  const outcomes = props.outcomes.map((i, idx) => (
    i ? (
      <View key={idx} style={styles.oneResultBody}>
        <Icon type='FontAwesome' name='check' style={styles.outcomeIcon}/>
      </View>
    ) : null
  ))
  return (
    <View>
      <View style={styles.outcomeContainer}>
        <View><Text style={styles.resultHeaderText}>{props.title}</Text></View>
        {outcomes}
      </View>
    </View>
  )
}

function OutcomesPoints(props) {
  const outcomes = props.outcomes.map((i, idx) => (
    <View key={idx} style={styles.oneResultBody}>
      <Text style={styles.outcomePointsText}>{i}</Text>
    </View>
  ))
  return (
    <View>
      <View style={styles.outcomeContainer}>
        <View><Text style={styles.resultHeaderText}>{props.title}</Text></View>
        {outcomes}
      </View>
    </View>
  )
}

function FighterTournamentOne(props) {
  let opponents = []
  let subs = []
  let wins = []
  let points = []
  props.tournament.timeline.forEach(i => {
    opponents.push(i.opponent)
    subs.push(i.result == 'submission')
    wins.push(i.result == 'submission' || i.result == 'win')
    points.push(i.rankingPoints == 0 ? '--' : '+' + i.rankingPoints)
  })

  return (
    <View style={styles.oneTournament}>
      <View style={props.isLast ? styles.oneTournamentContainerLast : styles.oneTournamentContainer}>
        <Header tournament={props.tournament} />
        <View style={styles.results}>
          <View style={styles.resultsContainer}>
            <Opponents opponents={opponents} />
            <View style={styles.allOutcomes}>
              <View style={styles.allOutcomesContainer}>
                <Outcomes key={0} title={'Sub'} outcomes={subs} />
                <Outcomes key={1} title={'Win'} outcomes={wins} />
                <OutcomesPoints title={'Points'} outcomes={points} />
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  )
}



function FighterTournaments(props) {
  const lastIdx = props.tournaments.length - 1
  const tournaments = props.tournaments.map((i, idx) => <FighterTournamentOne key={idx} tournament={i} isLast={idx == lastIdx} />)
  return (
    <View>
      <View style={styles.tournamentsContainer}>
        {tournaments}
      </View>
    </View>
  )
}

export default FighterTournaments


const baseStyles = {
  oneTournamentContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
}

const styles = EStyleSheet.create({
  tournamentsContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  oneTournament: {
    width: '100%',
  },
  oneTournamentContainer: baseStyles.oneTournamentContainer,
  oneTournamentContainerLast: {
    ...baseStyles.oneTournamentContainer,
    borderBottomWidth: 2,
    borderBottomColor: '#ededed',
  },
  header: {
    width: '100%',
  },
  headerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '70 * $vUnit',
    backgroundColor: '$bgColor',
  },
  headerTitle: {
    marginLeft: '40rem',
  },
  headerTitleText: {
    fontFamily: '$fontSemiBold',
  },
  medalPic: {
    width: '125rem',
    height: '125rem',
    marginLeft: '25 * $hUnit',
    marginRight: '25 * $hUnit',
  },
  results: {
    width: '100%',
    paddingTop: '12 * $vUnit',
    paddingBottom: '12 * $vUnit',
  },
  resultsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  opponents: {
    width: '45%',
    paddingLeft: '40rem',
  },
  opponentContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  opponentText: {
    fontFamily: '$fontReg',
    fontSize: '30rem',
  },
  allOutcomes: {
    width: '30%',
  },
  allOutcomesContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  outcomeContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  outcomeIcon: {
    fontSize: 16,
    color: '#2abd68',
    alignSelf: 'center',
  },
  outcomePointsText: {
    fontFamily: '$fontSemiBold',
    fontSize: '30rem',
    alignSelf: 'center',
  },
  oneResultBody: {
    marginTop: '25rem',
  },
  resultHeaderText: {
    fontFamily: '$fontMed',
    fontSize: '25rem',
  }
})