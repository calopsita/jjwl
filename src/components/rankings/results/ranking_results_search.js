import React, { useState } from 'react'
import { View, TextInput } from 'react-native'

import Icon from 'react-native-vector-icons/Feather'

import EStyleSheet from 'react-native-extended-stylesheet'

function MyFighterSearch(props) {
  const [searchText, setSearchText] = useState('')
  return (
    <View style={styles.parent}>
      <Icon name='search' style={styles.searchIcon}/>
      <TextInput style={styles.search}
        placeholder='Search Fighters'
        placeholderTextColor='#7d7c7e'
        value={props.searchText}
        onChangeText={(searchText) => setSearchText(searchText)}
      />
    </View>
  )
}

export default MyFighterSearch

const styles = EStyleSheet.create({
  parent: {
    flex: 1,
    flexDirection: 'row',
    width: '95%',
    height: '85rem',
    alignSelf: 'center',
    borderRadius: 5,
    backgroundColor: '#dbdbdb',
  },
  searchIcon: {
    color: '#7d7c7e',
    fontSize: '45rem',
    alignSelf: 'center',
    marginLeft: '15rem',
    marginRight: '15rem',
  },
  search: {
    fontFamily: '$fontReg',
    width: '100%',
    padding: 0,
  }
})