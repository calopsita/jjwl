import React from 'react'

import { ScrollView, View } from 'react-native'

import EStyleSheet from 'react-native-extended-stylesheet'

import MyHeader from '../../utils/header'
import FilterHeader from '../filter_header/filter_header'
import RankingResultsSearch from './ranking_results_search'
import RankingResultsFightersContainer from './fighters/ranking_results_fighters_container'

function RankingResults(props) {
  return (
    <View style={styles.parent}>
      <MyHeader title='Individual Rankings'/>
      <ScrollView
        contentContainerStyle={styles.scrollContainer}
      >
        <FilterHeader isFilterOpen={false} handleSelect={props.handleSelect}/>
        <RankingResultsSearch />
        <RankingResultsFightersContainer />
      </ScrollView>
    </View>
  )
}

export default RankingResults

const styles = EStyleSheet.create({
  parent: {
    flex: 1,
    backgroundColor: '$bgColor',
  },
  scrollContainer: {
    flexGrow: 1,
  },
})