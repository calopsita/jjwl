import React, { useState } from 'react'

import RankingResults from './results/ranking_results'
import RankingFilterContainer from './filter/ranking_filter_container'

function Rankings(props) {
  const [isFilterOpen, setIsFilterOpen] = useState(false)
  const handleSetIsFilterOpen = () => setIsFilterOpen(!isFilterOpen)

  if (isFilterOpen) {
    return (
      <RankingFilterContainer
        handleSelect={handleSetIsFilterOpen} 
      />
    )
  } else {
    return (
      <RankingResults
        handleSelect={handleSetIsFilterOpen} 
      />
    )
  }
}

export default Rankings