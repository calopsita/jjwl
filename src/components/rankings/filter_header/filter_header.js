import React from 'react'

import { View, ScrollView, TouchableOpacity } from 'react-native'

import Icon from 'react-native-vector-icons/Feather'

import EStyleSheet from 'react-native-extended-stylesheet'

import ChipGroupContainer from './chip/chip_group_container'
import ChipOtherContainer from './chip/chip_other_container'


function Chips(props) {
  const otherCats = [{ cat: 'a', title: 'age' }, { cat: 'b', title: 'belt' }, { cat: 'w', title: 'weight' }]
  const otherChips = otherCats.map(i => <ChipOtherContainer key={i.cat} cat={i.cat} title={i.title} mode={props.mode} />)
  
  return (
    <ScrollView
      horizontal={true}
      contentContainerStyle={styles.scrollContainer}
    >
      <ChipGroupContainer mode={props.mode}/>
      {otherChips}
    </ScrollView>
  )
}

function FilterHeader(props) {
  const mode = props.isFilterOpen ? 'pending' : 'active'
  return (
    <View>
      <View style={styles.parentContainer}>
        <TouchableOpacity onPress={props.handleSelect}>
          <View style={styles.iconContainer}>
            <View>
              <Icon name='filter' style={styles.filterIcon}/>
            </View>
          </View>
        </TouchableOpacity>
        <Chips mode={mode} />
      </View>
    </View>
  )
}

export default FilterHeader

const styles = EStyleSheet.create({
  parentContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
    height: '130rem',
    paddingLeft: '10rem',
  },
  iconContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  filterIcon: {
    color: 'black',
    fontSize: '95rem',
    alignSelf: 'center',
    marginLeft: '15rem',
    marginRight: '20rem',
  },
  scrollContainer: {
    flexGrow: 1,
  },
})