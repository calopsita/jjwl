import React from 'react'

import { connect } from 'react-redux'

import ChipClickModalContainer from './chip_click_modal_container'
import Chip from './chip'

class ChipGroupContainer extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const cats = [
      { cat: 'ageGroup', title: 'age group', data: [this.props.activeAgeGroupData] },
      { cat: 'gender', title: 'gender', data: [this.props.activeGenderData] }
    ]
    const chips = this.props.mode == 'results' ? (
      cats.map(i => <ChipClickModalContainer key={i.cat} cat={i.cat} title={i.title} data={i.data} />)
    ) : (
      cats.map(i => <Chip key={i.cat} title={i.title} data={i.data} />)
    )
    return (
      <React.Fragment>
        {chips}
      </React.Fragment>
    )
  }
}

function mapStateToProps(state, ownProps) {
  console.log('**ChipGroupContainer')
  console.log(ownProps)
  console.log(state)
  const active = state.rankFilter[ownProps.mode]['g']
  const categoryData = state.rankFilterCats.categoryData['g']
  console.log('!!!!')
  console.log(active)
  console.log(categoryData)
  return {
    activeAgeGroupData: categoryData[active].ageGroup,
    activeGenderData: categoryData[active].gender,
  }
}

export default connect(mapStateToProps)(ChipGroupContainer)