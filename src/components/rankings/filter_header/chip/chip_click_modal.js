import React from 'react'

import { TouchableOpacity } from 'react-native'

import Chip from './chip'

function ChipClickModal(props) {
  return (
    <TouchableOpacity onPress={props.updateFilterModal}>
      <Chip title={props.title} data={props.data}/>
    </TouchableOpacity>
  )
}

export default ChipClickModal