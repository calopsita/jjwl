import React from 'react'

import { connect } from 'react-redux'

import ChipClickModal from './chip_click_modal'

class ChipClickModalContainer extends React.Component {
  constructor(props) {
    super(props)

    this.updateFilterModal = this.updateFilterModal.bind(this)
  }

  updateFilterModal() {
    this.props.dispatch({
      type: 'UPDATE_RANK_FILTER_MODAL',
      cat: this.props.cat,
    })
  }

  render() {
    return (
      <ChipClickModal
        title={this.props.title}
        data={this.props.data}
        updateFilterModal={this.props.updateFilterModal}
      />
    )
  }
}

export default connect()(ChipClickModalContainer)