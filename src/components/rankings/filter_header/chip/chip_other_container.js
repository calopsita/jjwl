import React from 'react'

import { connect } from 'react-redux'

import ChipClickModalContainer from './chip_click_modal_container'
import Chip from './chip'

class ChipOtherContainer extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const chip = this.props.activeCategoryData.length > 0 ? (
      this.props.mode == 'results' ? (
        <ChipClickModalContainer cat={this.props.cat} title={this.props.title} data={this.props.activeCategoryData} />
      ) : <Chip cat={this.props.cat} title={this.props.title} data={this.props.activeCategoryData} />
    ) : null
    
    return chip
  }
}

function mapStateToProps(state, ownProps) {
  const active = state.rankFilter[ownProps.mode][ownProps.cat]
  const categoryData = state.rankFilterCats.categoryData[ownProps.cat]
  const activeCategoryData = [...active].map(i => categoryData[i])
  return {
    activeCategoryData: activeCategoryData,
  }
}

export default connect(mapStateToProps)(ChipOtherContainer)