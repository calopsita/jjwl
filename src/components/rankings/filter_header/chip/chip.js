import React from 'react'
import { View, Text } from 'react-native'

import EStyleSheet from 'react-native-extended-stylesheet'

function Chip(props) {
  const activeText = Object.keys(props.data).reduce((acc, i, idx) => {
    const d = props.data[i]
    const txt = idx == 0 ? d.n : ', ' + d.n
    return acc + txt
  }, '')

  return (
    <View style={styles.parent}>
      <View style={styles.group}>
        <View><Text style={styles.groupHeaderText}>{props.title}</Text></View>
        <View><Text style={styles.groupText}>{activeText}</Text></View>
      </View>
    </View>
  )
}

export default Chip

const styles = EStyleSheet.create({
  parent: {
    height: '100%',
  },
  group: {
    paddingLeft: '30rem',
    paddingRight: '30rem',
    paddingTop: '10rem',
    paddingBottom: '5rem',
    marginRight: '10rem',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: 'black',
  },
  groupHeaderText: {
    fontFamily: '$fontReg',
    fontSize: 10,
  },
  groupText: {
    fontFamily: '$fontReg',
  },
})