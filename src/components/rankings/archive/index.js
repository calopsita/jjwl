import React from 'react'

import { ScrollView, View, Text, Dimensions } from 'react-native'

import NativeMethodsMixin from 'NativeMethodsMixin'

import { connect } from 'react-redux'

import EStyleSheet from 'react-native-extended-stylesheet'

import MyHeader from '../utils/header'
import MyBeltSelect from './belt_select'
import MyFighterFilterContainer from './fighter_filter_container'
import MyFighterFilterFooter from './fighter_filter_footer'
import MyFighterSearch from './fighter_search'
import MyFighters from './fighters'


class RankingsScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    	expanded: {},
    	expandedFilterCat: null,
    	expandedFilterCatLast: null,
    }

    this.setScrollContainerRef = this.setScrollContainerRef.bind(this)
    this.setHeaderLayout = this.setHeaderLayout.bind(this)
    this.setBodyLayout = this.setBodyLayout.bind(this)
    this.setControlLayout = this.setControlLayout.bind(this)
    this.setFooterLayout = this.setFooterLayout.bind(this)
    this.setFilterComponentLayout = this.setFilterComponentLayout.bind(this)
    this.catExpandToggle = this.catExpandToggle.bind(this)
    this.alignFilterCatComponent = this.alignFilterCatComponent.bind(this)
    this.rankingClick = this.rankingClick.bind(this)
    this.yearClick = this.yearClick.bind(this)
  }

  componentDidMount() {
    this.props.dispatch({
      type: 'RETRIEVE_RANKINGS',
      payload: {}
    })
    this.props.dispatch({
      type: 'POST',
      task: 'categories_grouping',
      payload: {
        token: this.props.token,
      },
      nextTypeSuccess: 'RETRIEVE_RANK_FILTER_CATS_SUCCEEDED',
    })
  }

  componentDidUpdate(prevProps, prevState) {
  	if (this.props.filterExpanded) {
  		this.alignFilterCatComponent(this.state.expandedFilterCat, this.state.expandedFilterCatLast)
  	}
  }

  setScrollContainerRef(r) {
  	this.scrollContainerRef = r
  }

  setHeaderLayout(e) {
  	this.headerLayout = e.nativeEvent.layout
  }

  setBodyLayout(e) {
  	this.bodyLayout = e.nativeEvent.layout
  }

  setControlLayout(e) {
  	this.controlLayout = e.nativeEvent.layout
  }

  setFooterLayout(e) {
  	this.footerLayout = e.nativeEvent.layout
  }

  setFilterComponentLayout(cat, l) {
		if (cat == this.state.expandedFilterCat || cat == this.state.expandedFilterCatLast) {
			this.filterCatLayout = l
		}
  }

  catExpandToggle(cat) {
    this.setState(prevState => {
      if (prevState.expandedFilterCat == cat) {
        return {
          ...prevState,
          expandedFilterCat: null,
          expandedFilterCatLast: cat,
        }
      } else {
        return {
        	...prevState,
          expandedFilterCat: cat,
          expandedFilterCatLast: null,
        }
      }
    })
  }

  alignFilterCatComponent(cat, lastCat) {
    if (cat != null || lastCat != null) {
			NativeMethodsMixin.measure.call(this.scrollContainerRef, (x, y, width, height, px, py) => {
				const layout = this.filterCatLayout
				const scrollBottom = y + height
				const headerBottom = y + this.headerLayout.y + this.headerLayout.height
				const bodyTop = y + this.bodyLayout.y
				const controlTop = bodyTop + this.controlLayout.y
				const filterTop = y + this.bodyLayout.y + this.controlLayout.y + layout.y
				const filterBottom = filterTop + layout.height
				const overflow = filterBottom - scrollBottom
	  		if (cat != null) {
		  		if (overflow > 0) {
		  			const fromTop = filterTop - overflow
		  			if (fromTop < headerBottom) {
		  				const filterGap = filterTop - (headerBottom)
		  				this.scrollContainerRef.scrollTo({x: 0, y: filterGap})
		  			} else {
		  				this.scrollContainerRef.scrollTo({x: 0, y: overflow})
		  			}
		  		}
		  	} else if (lastCat != null) {
		  		const filterGap = filterTop - layout.y - controlTop
		  		this.scrollContainerRef.scrollTo({x: 0, y: filterGap})
		  	}
	    })	
		}
  }

  rankingClick(i) {
  	this.setState(prevState => {
  		if (prevState.expanded[i] != undefined) {
  			const { [i.toString()]: deleted, ...nextExpanded } = prevState.expanded
  			return {
  				expanded: nextExpanded
  			}
  		} else {
  			return {
  				expanded: Object.assign(prevState.expanded, {[i]: []})
  			}
  		}
  	})
  }

  yearClick(i, year) {
  	this.setState(prevState => {
  		const idx = prevState.expanded[i.toString()].indexOf(year)
  		let nextExpanded
  		if (idx > -1) {
  			nextExpanded = [...prevState.expanded[i].slice(0, idx), ...prevState.expanded[i].slice(idx+1)]
  			return {
  				expanded: Object.assign({}, prevState.expanded, {[i]: nextExpanded})
  			}
  		} else {
  			nextExpanded = prevState.expanded[i].slice()
  			nextExpanded.push(year)
  			return {
  				expanded: Object.assign({}, prevState.expanded, {[i]: nextExpanded})
  			}
  		}
  	})
  }

  render() {
    let body
    let filterFooter
    if (this.props.filterExpanded) {
    	body = <React.Fragment />
    	filterFooter = (
    		<View onLayout={this.setFooterLayout}>
    			<MyFighterFilterFooter />
    		</View>
    	)
    } else {
    	body = (
	      <React.Fragment>
		      <View style={styles.fighterSearchContainer}>
		        <MyFighterSearch />
		      </View>
		      <View style={styles.fighterContainer}>
		      	<MyFighters
		      		rankings={this.props.rankings}
		      		expanded={this.state.expanded}
		      		rankingClick={this.rankingClick}
		      		yearClick={this.yearClick}
		      	/>
		      </View>
		    </React.Fragment>
    	)
    	filterFooter = <View onLayout={this.setFooterLayout} />
    }
    return (
    	<View 
    		style={styles.screenContainer}>
	      <ScrollView
	      	ref={this.setScrollContainerRef}
	        stickyHeaderIndices={[0]}
	        contentContainerStyle={styles.scrollContainer}
	      >
	      	<View onLayout={this.setHeaderLayout}>
		        <MyHeader title='Individual Rankings'/>
		      </View>
	        <View onLayout={this.setBodyLayout}
	        	style={styles.fighterFilterContainer}>
	        	<MyFighterFilterContainer 
	        		expandedCat={this.state.expandedFilterCat}
	        		catExpandToggle={this.catExpandToggle}
	        		setControlLayout={this.setControlLayout}
	        		setFilterComponentLayout={this.setFilterComponentLayout}
	        	/>
	        </View>
	        {body}
	    	</ScrollView>
	    	{filterFooter}
	    </View>
    )
  }
}

function mapStateToProps(state) {
  return {
  	filterExpanded: state.rankFilter.expanded,
  	token: state.token,
  	rankings: state.rankings || [],
  }
}

export default connect(mapStateToProps)(RankingsScreen)



const styles = EStyleSheet.create({
  screenContainer: {
  	flex: 1,
  	backgroundColor: '$bgColor',
  },
  scrollContainer: {
    flexGrow: 1,
  	backgroundColor: '$bgColor',
  },
  fighterFilterContainer: {
    marginTop: '12 * $vUnit',
  },
  fighterSearchContainer: {
    marginTop: '3 * $vUnit',
  },
  fighterContainer: {
    marginTop: '18 * $vUnit',
  },
})


    // this.props.dispatch({
    //   type: 'POST',
    //   task: 'ranking_groups_users',
    //   payload: {
    //     token: this.props.token,
    //     level1: '4',
    //     level2: '8',
    //     level3: '6',
    //     level4: '2',
    //     year: '2018',
    //   },
    //   nextTypeSuccess: 'RETRIEVE_CATEGORIES_SUCCEEDED',
    // })
    // this.props.dispatch({
    //   type: 'POST',
    //   task: 'leages',
    //   payload: {
    //     token: this.props.token,
    //   },
    //   nextTypeSuccess: 'RETRIEVE_LEAGES_SUCCEEDED',
    // })
    // this.props.dispatch({
    //   type: 'POST',
    //   task: 'categories',
    //   payload: {
    //     token: this.props.token,
    //     rank: 'orgs',
    //     age: 0,
    //     leage: 4,
    //   },
    //   nextTypeSuccess: 'RETRIEVE_CATEGORIES_SUCCEEDED',
    // })
    // this.props.dispatch({
    //   type: 'POST',
    //   task: 'ranking',
    //   payload: {
    //     token: this.props.token,
    //     rank: 'orgs',
    //     age: 0,
    //     leage: 4,
    //   },
    //   nextTypeSuccess: 'RETRIEVE_RANKINGS_ORGS_SUCCEEDED',
    // })
		// this.props.dispatch({
		// 	type: 'POST',
		// 	task: 'ranking',
		// 	payload: {
		// 	  token: this.props.token,
		// 	  rank: 'gyms',
		// 	  age: 0,
		// 	  leage: 4,
		// 	},
		// 	nextTypeSuccess: 'RETRIEVE_RANKINGS_GYMS_SUCCEEDED',
		// })
		// this.props.dispatch({
		// 	type: 'POST',
		// 	task: 'ranking',
		// 	payload: {
		// 	  token: this.props.token,
		// 	  rank: 'users',
		// 	  age: 0,
		// 	  year: 2018,
		// 	},
		// 	nextTypeSuccess: 'RETRIEVE_RANKINGS_USERS_SUCCEEDED',
		// })

  // scrollheightChange(h) {
  // 	this.setState(prevState => {
  // 		if (this.props.filterExpanded && h > prevState.scrollheight) {
  // 			this.scrollContainerRef.scrollToEnd({animated: true})
  // 		} else if (this.props.filterExpanded && h < prevState.scrollheight) {
  // 			this.scrollContainerRef.scrollTo({y: 0, animated: true})
  // 		}
  // 		return {
  // 			scrollheight: h
  // 		}
  // 	})
  // }

  // onContentSizeChange={(w, h) => this.scrollheightChange(h)}