import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import { Icon } from 'native-base'

import EStyleSheet from 'react-native-extended-stylesheet'

import utils from './utils'
import MyFighterYearTournament from './fighter_year_tournament'

export default function MyFighterYear(props) {
	const medalCount = utils.getMedalCount(props.tournaments)
  let tournaments
  if (props.isExpanded == true) {
    tournaments = props.tournaments.map((i, idx) => {
      return (
        <MyFighterYearTournament key={idx} 
          tournament={i}
          last={idx == props.tournaments.length - 1}
        />
      )
    })
  } else {
    tournaments = null
  }

  return (
    <React.Fragment>
      <TouchableOpacity onPress={() => props.yearClick(props.fighterIdx, props.year)} style={styles.content}>
        <View>
          <View style={styles.summary}>
            <View style={styles.year}>
              <View style={styles.yearContainer}>
                <Text style={styles.yearText}>{props.year}</Text>
              </View>
            </View>
            <View style={styles.trophies}>
              <View style={styles.trophiesContainer}>
                <View>
                  <View style={styles.trophy}>
                    <View>
                      <Icon type='FontAwesome' name='trophy' style={{fontSize: 16, color: '#bf953f'}}/>
                    </View>
                    <View>
                      <Text style={styles.trophyText}>{medalCount['1']}</Text>
                    </View>
                  </View>
                </View>
                <View>
                  <View style={styles.trophy}>
                    <View>
                      <Icon type='FontAwesome' name='trophy' style={{fontSize: 16, color: '#c0c0c0'}}/>
                    </View>
                    <View>
                      <Text style={styles.trophyText}>{medalCount['2']}</Text>
                    </View>
                  </View>
                </View>
                <View>
                  <View style={styles.trophy}>
                    <View>
                      <Icon type='FontAwesome' name='trophy' style={{fontSize: 16, color: '#b87333'}}/>
                    </View>
                    <View>
                      <Text style={styles.trophyText}>{medalCount['3']}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.stats}>
              <View style={styles.statsContainer}>
                <View>
                  <View style={styles.statContainer}>
                    <Text style={styles.statHeader}>
                      Subs
                    </Text>
                    <Text style={styles.stat}>
                      {utils.getSubCount(props.tournaments)}
                    </Text>
                  </View>
                </View>
                <View>
                  <View style={styles.statContainer}>
                    <Text style={styles.statHeader}>
                      Wins
                    </Text>
                    <Text style={styles.stat}>
                      {utils.getWinCount(props.tournaments)}
                    </Text>
                  </View>
                </View>
                <View>
                  <View style={styles.statContainer}>
                    <Text style={styles.statHeader}>
                      Points
                    </Text>
                    <Text style={styles.stat}>
                      {utils.getRankPoints(props.tournaments)}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.bottomBorder} />
        <View style={styles.tournaments}>
          {tournaments}
        </View>
      </TouchableOpacity>
    </React.Fragment>
  )
}

const styles = EStyleSheet.create({
  content: {
    backgroundColor: 'white',
  },
  summary: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: '12rem',
  },
  year: {
    width: '20%',
    height: '100%',
  },
  yearContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  yearText: {
    fontFamily: '$fontMed',
    fontSize: '50rem',
  },
  trophies: {
    width: '25%',
    height: '100%',
  },
  trophiesContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: '40rem',
    paddingRight: '40rem',
    paddingTop: '12rem',
  },
  trophy: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  trophyText: {
    fontFamily: '$fontReg',
    fontSize: '25rem',
  },
  stats: {
    width: '55%',
    paddingLeft: '50rem',
    paddingRight: '50rem',
  },
  statsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  statContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  statHeader: {
    fontFamily: '$fontReg',
    fontSize: '25rem',
  },
  stat: {
    fontFamily: '$fontSemiBold',
    fontSize: '77rem',
    marginTop: '-3 * $vUnit',
  },
  bottomBorder : {
    borderBottomWidth: 2,
    borderBottomColor: '#ededed',
    width: '100%',
    alignSelf: 'center',
  },
  tournaments: {
    backgroundColor: '$bgColor',
  },
})