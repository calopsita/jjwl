import React from 'react'
import { View, TextInput } from 'react-native'

import { Icon } from 'native-base'

import EStyleSheet from 'react-native-extended-stylesheet'

export default function MyFighterSearch(props) {
  return (
    <View style={styles.searchContainer}>
      <Icon type='Feather' name='search' style={styles.searchIcon}/>
      <TextInput style={styles.search}
        placeholder='Search Fighters'
        placeholderTextColor='#7d7c7e'
      />
    </View>
	)
}

const styles = EStyleSheet.create({
  searchContainer: {
    flex: 1,
    flexDirection: 'row',
    width: '95%',
    height: '85rem',
    alignSelf: 'center',
    borderRadius: 5,
    backgroundColor: '#dbdbdb',
  },
  searchIcon: {
    color: '#7d7c7e',
    fontSize: '45rem',
    alignSelf: 'center',
    marginLeft: '15rem',
    marginRight: '15rem',
  },
  search: {
    fontFamily: '$fontReg',
    width: '100%',
    padding: 0,
  }
})