import React from 'react'
import { View, Text, TouchableHighlight } from 'react-native'

import withImmutablePropsToJS from 'with-immutable-props-to-js'

import LinearGradient from 'react-native-linear-gradient'

import { Thumbnail } from 'native-base'

import EStyleSheet from 'react-native-extended-stylesheet'

const gradientStart = {x: .4, y: .9}
const gradientEnd = {x: .2, y: .05}


export default function MyBeltSelect(props) {
  const beltThumbnails = props.options.map((i, idx) => {
    const adjGradientStart = {
      x: gradientStart.x + (.08 * idx),
      y: gradientStart.y - (.02 * idx),
    }
    if (i.active) {
      return (
        <View key={i.id}>
          <LinearGradient
            style={styles.insetShadow}
            colors={['#FFFFFF', '#D1D1D1', '#000000']}
            locations={[0, .4, .95]}
            start={{x: .3, y: .9}}
            end={{x: .3, y: .1}}
          >
            <TouchableHighlight 
              underlayColor='#FFFFFF'
              onPress={() => props.updateMultiCheck(props.cat, i.id)}
              style={[styles.oneBeltTouch, styles.activeOneBeltTouch]}>
                <View style={styles.oneBeltContainer}>
                  <Thumbnail
                    style={[styles.belt, styles[i.n.toLowerCase()+'Belt']]}
                  />
                </View>
            </TouchableHighlight>
          </LinearGradient>
        </View>
      )
    } else {
      return (
        <View key={i.id}>
          <TouchableHighlight 
            underlayColor='#000000'
            onPress={() => props.updateMultiCheck(props.cat, i.id)}
            style={[styles.oneBeltTouch, styles.inactiveOneBeltTouch]}>
              <View style={styles.oneBeltContainer}>
                <Thumbnail
                  style={[styles.belt, styles[i.n.toLowerCase()+'Belt']]}
                />
              </View>
          </TouchableHighlight>
        </View>
      )
    }
  })
  return (
    <View style={styles.cat}>
      <View style={styles.controlHeader}>
        <Text style={styles.controlHeaderText}>{props.title}</Text>
      </View>
      <View style={styles.beltSelectContainer}>
        {beltThumbnails}
      </View>
    </View>
	)
}

const styles = EStyleSheet.create({
  cat: {
    marginBottom: '$size6',
  },
  controlHeader: {
    marginTop: '$size5',
    marginBottom: '$size7',
    marginLeft: '$size7',
  },
  controlHeaderText: {
    fontFamily: '$fontMed',
  },
  beltSelectContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  oneBeltTouch: {
    borderWidth: '$size1',
    borderRadius: '$size10_5 / 2',
    borderColor: '#00000000',
  },
  activeOneBeltTouch: {
    height: '$size10_5 - $size1',
    width: '$size10_5 - $size1',
    backgroundColor: '$grey200',
  },
  inactiveOneBeltTouch: {
    height: '$size10_5',
    width: '$size10_5',
    backgroundColor: '$grey100',
  },
  insetShadow: {
    flex: 1,
    alignItems: 'center', 
    justifyContent: 'center',
    height: '$size10_5',
    width: '$size10_5',
    borderRadius: '$size10_5 / 2',
  },
  oneBeltContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems:'center',
  },
  belt: {
    width: '$size9_5',
    height: '$size9_5',
    borderRadius: '$size9_5 / 2',
    alignSelf: 'center',
  },
  whiteBelt: {
    backgroundColor: 'white',
    borderWidth: '$size1',
    borderColor: '$grey400',
  },
  greyBelt: {
    backgroundColor: '$grey500',
  },
  yellowBelt: {
    backgroundColor: 'yellow',
  },
  orangeBelt: {
    backgroundColor: 'orange',
  },
  blueBelt: {
    backgroundColor: '#5990DC',
  },
  purpleBelt: {
    backgroundColor: '#832FF4',
  },
  greenBelt: {
    backgroundColor: 'green',
  },
  brownBelt: {
    backgroundColor: '#65462A',
  },
  blackBelt: {
    backgroundColor: 'black',
  },
})