
function reducer(total, num) {
	return total + num
}

function compare(a, b) {
  let out
  if (a.rankPoints > b.rankPoints) {
    out = -1
  } else if (a.rankPoints < b.rankPoints) {
    out = 1
  } else if (a.name < b.name) {
    out = -1
  } else if (a.name > b.name) {
    out = 1
  } else {
  	out = 0
  }
  return out
}

export default class utils {

  static getRankPoints(tournaments) {
    return tournaments.map(i => i.timeline.map(ii => ii.rankingPoints)).flat().reduce(reducer)
  }

  static getWinCount(tournaments) {
    return tournaments.map(i => i.timeline.filter(ii => ii.result == 'win')).flat().length
  }

  static getSubCount(tournaments) {
    return tournaments.map(i => i.timeline.filter(ii => ii.result == 'submission')).flat().length
  }

  static getMedalCount(tournaments) {
    const allMedals = tournaments.map(i => i.medal.rank)
    let medals = [1, 2, 3]
    let medalCount = {}
    medals.map(i => medalCount[i.toString()] = allMedals.filter(ii => ii = i).length)
    return medalCount
  }

  static sort(rankings) {
    return rankings.slice().sort(compare)
  }

}

