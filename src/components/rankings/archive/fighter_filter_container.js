import React from 'react'

import { Animated, Easing } from 'react-native'

import { connect } from 'react-redux'

import EStyleSheet from 'react-native-extended-stylesheet'

import makeGetCatOptions from '../../data/selectors/cat_options'

import MyFighterFilter from './fighter_filter'

class MyFighterFilterContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}

    this.controlExpandToggle = this.controlExpandToggle.bind(this)
    this.updateMultiCheck = this.updateMultiCheck.bind(this)
    this.updateRadio = this.updateRadio.bind(this)
  }

  controlExpandToggle() {
    console.log('controlToggle')
    this.props.dispatch({
      type: 'TOGGLE_FILTER',
    })
  }

  updateMultiCheck(cat, id) {
    this.props.dispatch({
      type: 'UPDATE_FILTER_MULTICHECK',
      cat: cat,
      id: id,
    })
  }

  updateRadio(cat, id) {
    this.props.dispatch({
      type: 'UPDATE_FILTER_RADIO',
      cat: cat,
      id: id,
    })
  }

  render() {
    let ageGroupOptions = [
      {n: 'Adults', id: 0},
      {n: 'Youth', id: 1},
    ]
    ageGroupOptions = ageGroupOptions.map(i => ({...i, ...{active: i.id == this.props.rankFilter.active.ageGroup}}))
    let genderOptions = [
      {n: 'Male', id: 0},
      {n: 'Female', id: 1},
    ]
    genderOptions = genderOptions.map(i => ({...i, ...{active: i.id == this.props.rankFilter.active.gender}}))
    const ageOptions = this.props.catOptionsUnique.get('age').map(i => ({...i.toJS(), ...{active: this.props.rankFilter.active.age.has(i.get('id'))}}))
    const beltOptions = this.props.catOptionsUnique.get('belt').map(i => ({...i.toJS(), ...{active: this.props.rankFilter.active.belt.has(i.get('id'))}}))
    const weightOptions = this.props.catOptionsUnique.get('weight').map(i => ({...i.toJS(), ...{active: this.props.rankFilter.active.weight.has(i.get('id'))}}))
    let seasonOptions = [
      {n: '2018', id: 0},
      {n: '2017', id: 1},
      {n: '2016', id: 2}, 
    ]
    seasonOptions = seasonOptions.map(i => ({...i, ...{active: this.props.rankFilter.active.season.has(i.id)}}))

    return (
      <MyFighterFilter
        ageGroupOptions={ageGroupOptions}
        genderOptions={genderOptions}
        beltOptions={beltOptions}
        ageOptions={ageOptions}
        weightOptions={weightOptions}
        seasonOptions={seasonOptions}
        expanded={this.props.rankFilter.expanded}
        expandedCat={this.props.expandedCat}
        controlExpandToggle={this.controlExpandToggle}
        updateMultiCheck={this.updateMultiCheck}
        updateRadio={this.updateRadio}
        setControlLayout={this.props.setControlLayout}
        setFilterComponentLayout={this.props.setFilterComponentLayout}
        catExpandToggle={this.props.catExpandToggle}
      />
    )
  }
}

const makeMapStateToProps = () => {
  getCatOptions = makeGetCatOptions()
  const mapStateToProps = (state) => {
    return {
      rankFilterCats: state.rankFilterCats,
      rankFilter: state.rankFilter,
      catOptionsUnique: getCatOptions(state)
    }
  }
  return mapStateToProps
}

export default connect(makeMapStateToProps)(MyFighterFilterContainer)

const styles = EStyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
    backgroundColor: '$bgColor',
  },
  beltSelectContainer: {
    marginTop: '12 * $vUnit',
  },
  fighterFilterContainer: {
    marginTop: '12 * $vUnit',
  },
  fighterSearchContainer: {
    marginTop: '12 * $vUnit',
  },
  fighterContainer: {
    marginTop: '12 * $vUnit',
  },
})