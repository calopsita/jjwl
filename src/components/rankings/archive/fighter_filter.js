import React from 'react'
import { View, Text, ScrollView, TouchableOpacity, Animated, Easing } from 'react-native'

import withImmutablePropsToJS from 'with-immutable-props-to-js'

import { Icon } from 'native-base'

import SectionedMultiSelect from 'react-native-sectioned-multi-select'

import EStyleSheet from 'react-native-extended-stylesheet'

import { MyRadio } from './radio'
import { MyHeaderChip } from './header_chip'
import { MyMultiSelect } from './filter_multiselect'
import MyBeltSelect from './belt_select'
import MyCheckbox from './checkbox'

const MyFighterFilter = (props) => {
  const ageGroupOptionsActive = props.ageGroupOptions.filter(i => i.active)
  const genderOptionsActive = props.genderOptions.filter(i => i.active)
  const beltOptionsActive = props.beltOptions.filter(i => i.active)
  const ageOptionsActive = props.ageOptions.filter(i => i.active)
  const weightOptionsActive = props.weightOptions.filter(i => i.active)
  const seasonOptionsActive = props.seasonOptions.filter(i => i.active)

  const border = props.expanded ? (
    <View style={styles.bottomBorder} />
  ) : <View />

  const control = props.expanded ? (
    <View 
      onLayout={props.setControlLayout} 
      style={styles.control}>
      <View>
        <View style={styles.catContainer}>
          <MyRadio
            cat={'ageGroup'}
            title={'age group'}
            options={props.ageGroupOptions}
            updateRadio={props.updateRadio}
            index={0}
            count={2}
          />
          <MyRadio
            cat={'gender'}
            title={'gender'}
            options={props.genderOptions}
            updateRadio={props.updateRadio}
            index={1}
            count={2}
          />
          <MyMultiSelect
            cat={'age'}
            title={'age'}
            expanded={props.expandedCat == 'age'}
            options={props.ageOptions}
            optionsActive={ageOptionsActive}
            updateMultiCheck={props.updateMultiCheck}
            catExpandToggle={props.catExpandToggle}
            setFilterComponentLayout={props.setFilterComponentLayout}
          />
          <MyMultiSelect
            cat={'weight'}
            title={'weight'}
            expanded={props.expandedCat == 'weight'}
            options={props.weightOptions}
            optionsActive={weightOptionsActive}
            updateMultiCheck={props.updateMultiCheck}
            catExpandToggle={props.catExpandToggle}
            setFilterComponentLayout={props.setFilterComponentLayout}
          />
          <MyBeltSelect
            cat={'belt'}
            title={'belt'}
            options={props.beltOptions}
            updateMultiCheck={props.updateMultiCheck}
          />
          <MyMultiSelect
            cat={'season'}
            title={'season'}
            expanded={props.expandedCat == 'season'}
            options={props.seasonOptions}
            optionsActive={seasonOptionsActive}
            updateMultiCheck={props.updateMultiCheck}
            catExpandToggle={props.catExpandToggle}
            setFilterComponentLayout={props.setFilterComponentLayout}
          />
        </View>
      </View>
    </View>
  ) : <View onLayout={props.setControlLayout} />

  return (
    <React.Fragment>
      <View style={styles.filterContainer}>
        <TouchableOpacity onPress={() => props.controlExpandToggle()}>
          <View style={styles.iconContainer}>
            <View>
              <Icon type='Feather' name='filter' style={styles.filterIcon}/>
            </View>
          </View>
        </TouchableOpacity>
        <ScrollView
          horizontal={true}
          style={styles.summary}
        >
        <View style={styles.summaryContainer}>
          <MyHeaderChip
            active={ageGroupOptionsActive}
            title={'age group'}
          />
          <MyHeaderChip
            active={genderOptionsActive}
            title={'gender'}
          />
          <MyHeaderChip
            active={beltOptionsActive}
            title={'belt'}
          />
          <MyHeaderChip
            active={ageOptionsActive}
            title={'age'}
          />
          <MyHeaderChip
            active={weightOptionsActive}
            title={'weight'}
          />
          <MyHeaderChip
            active={seasonOptionsActive}
            title={'season'}
          />
        </View>
        </ScrollView>
      </View>
      {border}
      {control}
    </React.Fragment>
	)
}

export default withImmutablePropsToJS(MyFighterFilter)

const styles = EStyleSheet.create({
  filterContainer: {
    flex: 1,
    flexDirection: 'row',
    width: '95%',
    height: '130rem',
    alignSelf: 'center',
  },
  iconContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: '10rem',
  },
  filterIcon: {
    color: 'black',
    fontSize: '90rem',
    alignSelf: 'center',
    marginLeft: '15rem',
    marginRight: '15rem',
  },
  summary: {
    width: '80%',
  },
  summaryContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: '110rem',
    marginLeft: '20rem',
  },
  bottomBorder : {
    borderBottomWidth: 2,
    borderBottomColor: '#ededed',
    width: '100%',
    alignSelf: 'center',
  },
  control: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#F9F9F9'
  },
  controlHeader: {
    marginTop: '20rem',
    marginBottom: '30rem',
    marginLeft: '50rem',
  },
  controlHeaderText: {
    fontFamily: '$fontMed',
  },
  catContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  cat: {
    marginTop: '30rem',
  },
})