import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import { Icon } from 'native-base'

import EStyleSheet from 'react-native-extended-stylesheet'

import MyCheckbox from './checkbox'
  
class MyMultiSelect extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}

    this.onLayout = this.onLayout.bind(this)
    this.onPress = this.onPress.bind(this)
  }

  onLayout(e) {
    this.layout = e.nativeEvent.layout
    this.props.setFilterComponentLayout(this.props.cat, this.layout)
  }

  onPress() {
    this.props.catExpandToggle(this.props.cat, this.layout)
  }

  render() {
    let selects
    let dropdownIcon
    let chips
    if (this.props.expanded) {
      selects = this.props.options.map(i => (
        <View key={i.id} style={styles.dropdownSelect}>
          <View style={styles.dropdownContentContainer}>
            <MyCheckbox
              scale={1.1}
              active={i.active}
              toggle={() => this.props.updateMultiCheck(this.props.cat, i.id)}
            />
            <Text style={styles.dropdownSelectText}>{i.n}</Text>
          </View>
        </View>
      ))
      dropdownIcon = <Icon type='Feather' name='chevron-up' style={styles.dropdownChevron}/>
      chips = null
    } else {
      selects = null
      dropdownIcon = <Icon type='Feather' name='chevron-down' style={styles.dropdownChevron}/>
      chips = this.props.optionsActive.length == 0 ? null : (
        <View style={styles.filterChipSetContainer}>
          {this.props.optionsActive.map(i => (
            <View key={i.id}>
              <View style={styles.filterChipContainer}>
                <Text style={styles.filterChipText}>{i.n}</Text>
                <TouchableOpacity
                  onPress={() => this.props.updateMultiCheck(this.props.cat, i.id)}
                >
                  <View style={styles.filterChipCloseContainer}>
                    <View>
                      <Icon type='Feather' name='x' style={styles.filterChipClose}/>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          ))}
        </View>
      )
    }
    return (
      <View
        onLayout={this.onLayout}
        style={styles.cat}>
        <TouchableOpacity onPress={this.onPress}>
          <View style={styles.controlHeader}>
            <View style={styles.dropdownContainer}>
              <Text style={styles.controlHeaderText}>{this.props.title}</Text>
              {dropdownIcon}
            </View>
          </View>
        </TouchableOpacity>
        <View>
          {selects}
        </View>
        {chips}
      </View>
    )
  }
}

export { MyMultiSelect }

const styles = EStyleSheet.create({
  cat: {
    marginBottom: '$size6',
  },
  controlHeader: {
    marginTop: '$size5',
    marginBottom: '$size7',
    marginLeft: '$size7',
  },
  controlHeaderText: {
    fontFamily: '$fontMed',
  },
  dropdownContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  dropdownChevron: {
    fontSize: '$fontSize3',
    alignSelf: 'flex-end',
    marginLeft: '$size5',
  },
  dropdownSelect: {
    marginLeft: '$size8',
    marginBottom: '$size5',
  },
  dropdownContentContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  dropdownSelectText: {
    fontFamily: '$fontReg',
    fontSize: '$fontSize3',
    marginLeft: '$size3',
  },
  filterChipSetContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: '$size5 * -1',
    marginLeft: '$size6',
  },
  filterChipContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '$grey200',
    borderRadius: '$size8',
    borderWidth: '$size1',
    borderColor: 'black',
    marginTop: '$size4',
    marginRight: '$size5',
    paddingTop: '$size4',
    paddingBottom: '$size4',
    paddingLeft: '$size6',
    paddingRight: '$size4',
  },
  filterChipText: {
    fontFamily: '$fontReg',
    marginRight: '$size5'
  },
  filterChipCloseContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '$fontSize6',
    width: '$fontSize6',
    borderRadius: '$fontSize6 / 2',
    borderWidth: '$size1',
    borderColor: '$grey700',
    backgroundColor: '$grey000',
  },
  filterChipClose: {
    fontSize: '$fontSize4',
    textAlign: 'center',
    paddingBottom: '$size1',
  }
})