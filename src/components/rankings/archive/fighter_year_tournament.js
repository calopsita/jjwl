import React from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'

import Icon from 'react-native-vector-icons/Feather'

import EStyleSheet from 'react-native-extended-stylesheet'

export default function MyFighterYearTournament(props) {
  const medal = props.tournament.medal.rank ? (
    <View style={styles.medal}>
      <Image
        source={{uri: props.tournament.medal.img}}
        style={styles.img}
      />
    </View>
  ) : null
  let opponents = []
  let subs = []
  let wins = []
  let points = []
  for (let i = 0; i < props.tournament.timeline.length; i++) {
    const t = props.tournament.timeline[i]
    const opponent = (
      <View style={styles.timelineBody}>
        <Text style={styles.timelineOpponentText}>{t.opponent}</Text>
      </View>
    )
    opponents.push(opponent)
    const sub = t.result == 'submission' ? (
      <View style={styles.timelineBody}>
        <Icon name='check' style={{fontSize: 16, color: '#2abd68', alignSelf: 'center'}}/>
      </View>
    ) : null
    subs.push(sub)
    const win = (t.result == 'submission' || t.result == 'win') ? (
      <View style={styles.timelineBody}>
        <Icon name='check' style={{fontSize: 16, color: '#2abd68', alignSelf: 'center'}}/>
      </View>
    ): null
    wins.push(win)
    const point = t.rankingPoints == 0 ? (
      <View style={styles.timelineBody}>
        <Text style={styles.timelinePointText}>{'--'}</Text>
      </View>
    ) : (
      <View style={styles.timelineBody}>
        <Text style={styles.timelinePointText}>{'+'+t.rankingPoints}</Text>
      </View>
    )
    points.push(point)
  }
  const contentStyle = props.last ? styles.contentLast : styles.content

  return (
    <View style={contentStyle}>
      <View style={styles.headerContainer}>
        <View style={styles.header}>
          <Text style={styles.headerText}>{props.tournament.name}</Text>
        </View>
        {medal}
      </View>
      <View style={styles.timelineHeaderContainer}>
        <View style={styles.opponent}>
          <View style={styles.opponentContainer}>
            <View>
              <Text style={styles.timelineHeaderText}>Opponent</Text>
            </View>
            {opponents}
          </View>
        </View>
        <View style={styles.result}>
          <View style={styles.resultHeaderContainer}>
            <View>
              <View style={styles.resultContainer}>
                <Text style={styles.timelineHeaderText}>Sub</Text>
                {subs}
              </View>
            </View>
            <View style={styles.resultContainer}>
              <View>
                <Text style={styles.timelineHeaderText}>Win</Text>
                {wins}
              </View>
            </View>
            <View style={styles.resultContainer}>
              <View>
                <Text style={styles.timelineHeaderText}>Points</Text>
                {points}
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  )
 }

 const styles = EStyleSheet.create({
  content: {
    backgroundColor: '$bgColor',
  },
  contentLast: {
    backgroundColor: '$bgColor',
    marginBottom: '20 * $vUnit',
  },
  headerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '70 * $vUnit',
    backgroundColor: '$bgColor',
    marginBottom: '20 * $vUnit',
  },
  header: {
    marginLeft: '40rem',
  },
  headerText: {
    fontFamily: '$fontSemiBold',
  },
  medal: {
    marginRight: '40rem'
  },
  img: {
    width: '125rem',
    height: '125rem',
    marginLeft: '20rem',
  },
  timelineHeaderContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '$bgColor',
  },
  timelineHeaderText: {
    fontFamily: '$fontMed',
    fontSize: '25rem',
  },
  opponent: {
    width: '45%',
    paddingLeft: '40rem',
  },
  opponentContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  timelineBody: {
    marginTop: '25rem',
  },
  timelineOpponentText: {
    fontFamily: '$fontReg',
    fontSize: '30rem',
  },
  timelinePointText: {
    fontFamily: '$fontSemiBold',
    fontSize: '30rem',
    alignSelf: 'center',
  },
  result: {
    width: '55%',
    paddingLeft: '50rem',
    paddingRight: '50rem',
  },
  resultHeaderContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  resultContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  sub: {
  },
  win: {
  },
  points: {
  },


 })