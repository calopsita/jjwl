import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import { Icon } from 'native-base'

import { Thumbnail } from 'native-base'

import EStyleSheet from 'react-native-extended-stylesheet'

import utils from './utils'
import MyFighterYear from './fighter_year'

export default function MyFighters(props) {
  let summaryData = {}
  const rankingsPrepped = props.rankings.map(i => {
    if (i.timelines != undefined) {
      i.rankPoints = utils.getRankPoints(i.timelines['2018'])
      i.medalCount = utils.getMedalCount(i.timelines['2018'])
    }
    return i
  })
  const rankingsSorted = utils.sort(props.rankings)
  let lastPoints
  let lastRank
  const rankingEntries = rankingsSorted.map((i, idx) => {
    const rank = lastPoints && lastPoints == i.rankPoints ? lastRank : idx + 1
    lastPoints = i.rankPoints
    lastRank = rank
    const isExpanded = (i.timelines != undefined && props.expanded[idx] != undefined) ? true : false
    const isLastExpanded = (idx > 0 && rankingsSorted[idx-1].timelines != undefined && props.expanded[idx-1] != undefined) ? true : false
    let yearSummaries
    if (isExpanded == true) {
      const sortedTimelines = Object.keys(i.timelines).sort((a, b) => parseInt(b) - parseInt(a))
      yearSummaries = sortedTimelines.map((ii, iidx) => {
        return (
          <MyFighterYear key={ii}
            fighterIdx={idx}
            year={ii}
            tournaments={i.timelines[ii]}
            isExpanded={props.expanded[idx].indexOf(ii) > -1}
            yearClick={props.yearClick}
            last={iidx == (sortedTimelines.length - 1)}
          />
        )
      })
    } else {
      yearSummaries = null
    }
    return (
      <React.Fragment key={idx}>
        <TouchableOpacity onPress={() => props.rankingClick(idx)} style={isExpanded && styles.entryContainerExpanded || isLastExpanded && styles.entryContainerLastExpanded || styles.entryContainer}>
          <View style={styles.avatarContainer}>
            <Thumbnail source={{uri: i.img}} style={styles.avatar}/>
          </View>
          <View style={isExpanded && styles.infoContainerExpanded || styles.infoContainer}>
            <View style={styles.infoSubContainer}>
              <View>
                <View style={styles.infoTextContainer}>
                  <Text style={styles.infoHeader}>
                    {rank + ' - ' + i.name}
                  </Text>
                  <Text style={styles.infoSubHeader}>
                    {i.gymName}
                  </Text>
                </View>
              </View>
              <View style={styles.infoStatsContainer}>
                <View style={styles.infoStats}>
                  <View style={styles.trophyContainer}>
                    <View style={styles.trophy}>
                      <View>
                        <Icon type='FontAwesome' name='trophy' style={{fontSize: 16, color: '#bf953f'}}/>
                      </View>
                      <View>
                        <Text style={styles.trophyText}>{i.medalCount['1']}</Text>
                      </View>
                    </View>
                    <View style={styles.trophy}>
                      <View>
                        <Icon type='FontAwesome' name='trophy' style={{fontSize: 16, color: '#c0c0c0'}}/>
                      </View>
                      <View>
                        <Text style={styles.trophyText}>{i.medalCount['2']}</Text>
                      </View>
                    </View>
                    <View style={styles.trophy}>
                      <View>
                        <Icon type='FontAwesome' name='trophy' style={{fontSize: 16, color: '#b87333'}}/>
                      </View>
                      <View>
                        <Text style={styles.trophyText}>{i.medalCount['3']}</Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.infoStatsSubContainer}>
                    <Text style={styles.statsHeader}>
                      Points
                    </Text>
                    <Text style={styles.statsSubHeader}>
                      {i.rankPoints}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </TouchableOpacity>
        <View style={isExpanded && styles.yearSummary || {}}>
          {yearSummaries}
        </View>
      </React.Fragment>
    )
  })
  return (
    <View style={styles.rankingContainer}>
      {rankingEntries}
    </View>
	)
}

const styles = EStyleSheet.create({
  rankingContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  touchable: {
    paddingBottom: '5 * $vUnit',
  },
  entryContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: '100%',
    paddingLeft: '20rem',
    backgroundColor: 'white',
    paddingBottom: '5 * $vUnit',
    shadowOffset: { width: 0, height: 3 },
    shadowRadius: 0,
    shadowOpacity: 0,
    elevation: 5,
  },
  entryContainerExpanded: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: '100%',
    paddingLeft: '20rem',
    backgroundColor: 'white',
    paddingBottom: '5 * $vUnit',
    shadowOffset: { width: 0, height: 3 },
    shadowRadius: 1,
    shadowOpacity: .3,
    elevation: 5,
  },
  entryContainerLastExpanded: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: '100%',
    paddingLeft: '20rem',
    backgroundColor: 'white',
    paddingTop: '5 * $vUnit',
    paddingBottom: '5 * $vUnit',
    shadowOffset: { width: 0, height: -1 },
    shadowRadius: 1,
    shadowOpacity: .2,
    elevation: 5,
  },
  avatarContainer: {
    width: '15%',
  },
  avatar: {
  },
  infoContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#ededed',
    width: '85%',
    height: '100%',
    paddingLeft: '40rem',
  },
  infoContainerExpanded: {
    width: '85%',
    height: '100%',
    paddingLeft: '40rem',
  },
  trophy: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  trophyText: {
    fontFamily: '$fontReg',
    fontSize: '25rem',
    marginLeft: '10rem',
  },
  infoSubContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  infoTextContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  infoHeader: {
    fontFamily: '$fontReg',
    fontSize: '40rem',
    color: '#8b8b8b',
    marginBottom: '3 * $vUnit',
  },
  infoSubHeader: {
    fontFamily: '$fontReg',
    fontSize: '30rem',
    color: '#c7c7c7',
  },
  infoStatsContainer: {
    width: '30%',
  },
  infoStats: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  infoStatsSubContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  statsHeader: {
    fontFamily: '$fontReg',
    fontSize: '25rem',
  },
  statsSubHeader: {
    fontFamily: '$fontSemiBold',
    fontSize: '77rem',
    marginTop: '-3 * $vUnit',
  },
  yearSummary: {
    marginTop: '5 * $vUnit',
  }
})