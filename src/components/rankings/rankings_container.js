import React from 'react'

import { connect } from 'react-redux'

import Rankings from './rankings'

class RankingsContainer extends React.Component {
  constructor(props) {
    super(props)    
  }

  componentDidMount() {
    this.props.dispatch({
      type: 'POST',
      task: 'categories_grouping',
      payload: {
        token: this.props.token,
      },
      nextTypeSuccess: 'RETRIEVE_RANK_FILTER_CATS_SUCCEEDED',
    })
  }

  render() {
    return (
      <Rankings />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    token: state.token,
  }
}

export default connect(mapStateToProps)(RankingsContainer)