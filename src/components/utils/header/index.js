import React from 'react'
import { StatusBar } from 'react-native'
import { Container, Header, Left, Right, Body, Button, Title } from 'native-base'

import Icon from 'react-native-vector-icons/Feather'

import EStyleSheet from 'react-native-extended-stylesheet'

export default function MyHeader(props) {
  return (
    <Header style={styles.header}>
      <StatusBar barStyle='light-content' backgroundColor='black'/>
      <Left style={styles.left} />
      <Body style={styles.body}>
        <Title style={styles.title}>
          {props.title}
        </Title>
      </Body>
      <Right style={styles.right}>
        <Button transparent style={styles.addIconButton}>
          <Icon name='user-plus' style={styles.addIcon}/>
        </Button>
        <Button transparent style={styles.friendsIconButton}>
          <Icon name='users' style={styles.friendsIcon}/>
        </Button>
      </Right>
    </Header>
  )
}

const styles = EStyleSheet.create({
  header: {
    backgroundColor: '#181818',
    elevation: 10,
  },
  left: {
  },
  body: {
  },
  right: {
  },
  title: {
    width: '500rem',
    color: '$bgColor',
    fontFamily: '$fontMed',
  },
  addIconButton: {
    paddingRight: '35rem',
    paddingLeft: '35rem',
  },
  addIcon: {
    color: '$bgColor',
    fontSize: '60rem',
  },
  friendsIconButton: {
    paddingRight: '35rem',
    paddingLeft: '35rem',
  },
  friendsIcon: {
    color: '$bgColor',
    fontSize: '60rem',
  }
})