import React from 'react'

import { Segment, Button, Text  } from 'native-base'

import EStyleSheet from 'react-native-extended-stylesheet'

export default function MySubheader(props) {
  const buttonWidth = (100/props.options.length) + '%'
  console.log('MySubheader')
  const buttons = props.options.map((option, i) => {
    const buttonStyle = Object.assign({},
      props.index == i ? styles.activeModeButton : styles.inactiveModeButton,
      {width: buttonWidth}
    )
    const textStyle = props.index == i ? styles.activeModeButtonText : styles.inactiveModeButtonText
    console.log(buttonStyle)
    return (
      <Button 
        key={i}
        first={i == 0} 
        last={i == props.options.length - 1} 
        style={buttonStyle} 
        onPress={() => props.setIndex(i)}>
        <Text style={textStyle}>
          {option}
        </Text>
      </Button>
    )
  })
  return (
    <Segment style={styles.segment}>
      {buttons}
    </Segment>
  )
}

const styles = EStyleSheet.create({
  segment: {
    height: '100%',
    width: '100%',
    backgroundColor: '$bgColor'
  },
  inactiveModeButton: {
    justifyContent: 'center',
    borderColor: '#000000',
    backgroundColor: '$bgColor',
  },
  activeModeButton: {
    justifyContent: 'center',
    borderColor: '#000000',
    backgroundColor: '#000000',
  },
  inactiveModeButtonText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 15,
    color: '#000000',
    paddingLeft: 10,
    paddingRight: 10,
  },
  activeModeButtonText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 15,
    color: '$bgColor',
    paddingLeft: 10,
    paddingRight: 10,
  }
})
