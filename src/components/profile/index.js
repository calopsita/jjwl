import React from 'react'
import { ScrollView, Image, ImageBackground, View, Text } from 'react-native'

import { connect } from 'react-redux'

import EStyleSheet from 'react-native-extended-stylesheet'

import MyHeader from '../utils/header'
import MySubheader from '../utils/subheader'
import MySummary from './summary'
import MyRecord from './record'
import MyMedals from './medals'
import MyTimeline from './timeline'
import MyMedia from './media'

class ProfileScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      subheaderIndex: 0
    }
    this.setSubheaderIndex = this.setSubheaderIndex.bind(this)
  }

  componentDidMount() {
    this.props.dispatch({
      type: 'POST',
      task: 'profile',
      payload: {
        token: this.props.token,
        userID: 0,
      },
      nextTypeSuccess: 'RETRIEVE_USER_PROFILE_SUCCEEDED',
    })
  }

  setSubheaderIndex(i) {
    this.setState({subheaderIndex: i})
  }


  render() {
    console.log('***profile')
    console.log(this.props)

    const subheaderOptions = ['Profile', 'Your Events', 'Edit Profile']
    return (
      <ScrollView 
        stickyHeaderIndices={[0]}
        contentContainerStyle={styles.scrollContainer}
      >
        <MyHeader title='My Profile'/>
        <View style={styles.subheaderContainer}>
          <MySubheader 
            options={subheaderOptions}
            index={this.state.subheaderIndex}
            setIndex={this.setSubheaderIndex}
          />
        </View>
        <View>
          <ImageBackground
            style={styles.avatarContainer}
            imageStyle={styles.avatar} 
            source={{uri: this.props.avatar}} 
          >
            <View style={styles.avatarForegroundContainer}>
              <View style={styles.gymLogoContainer}>
                <Image
                  style={styles.gymLogo}
                  source={{uri: this.props.gymLogo}}
                />
              </View>
              <View style={styles.summaryContainer}>
                <MySummary
                  name={this.props.name}
                  surname={this.props.surname}
                  gymName={this.props.gymName}
                  rankPosition={this.props.rankPosition}
                />
              </View>
              <View style={styles.recordContainer}>
                <MyRecord
                  points={this.props.points}
                  victories={this.props.victories}
                  submissions={this.props.submissions}
                />
              </View>
              <View style={styles.medalsContainer}>
                <Text style={styles.sectionHeader}>
                  Medals
                </Text>
                <MyMedals 
                  medals={this.props.medals}
                />
              </View>
              <View style={styles.timelineContainer}>
                <Text style={styles.sectionHeader}>
                  Timeline
                </Text>
                <MyTimeline 
                  fights={this.props.fights}
                />
              </View>
              <View style={styles.mediaContainer}>
                <Text style={styles.sectionHeader}>
                  Media
                </Text>
                <MyMedia 
                  media={this.props.media}
                />
              </View>
            </View>
          </ImageBackground>
        </View>
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  console.log('mstp profile')
  console.log(state)
  return {
    token: state.token,
    avatar: state.profile.avatar,
    name: state.profile.name,
    surname: state.profile.surname,
    gymName: state.profile.gymName,
    gymLogo: state.profile.gymLogo,
    rankPosition: state.profile.rankPosition,
    points: state.profile.points,
    victories: state.profile.victories,
    submissions: state.profile.submissions,
    medals: state.profile.medals || [],
    fights: state.profile.fights || [],
    media: state.profile.media || [],
  }
}

export default connect(mapStateToProps)(ProfileScreen)


const styles = EStyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
    backgroundColor: '$bgColor',
  },
  subheaderContainer: {
    justifyContent: 'flex-start',
    alignSelf: 'center',
    width: '96%',
    height: '60 * $vUnit',
    marginTop: '0 * $vUnit',
    marginBottom: '0 * $vUnit',
    backgroundColor: '$bgColor',
  },
  avatarContainer: {
    width: '100%',
    height: '100%',
  },
  avatar: {
    position: 'absolute',
    height: '220 * $vUnit',
  },
  avatarForegroundContainer: {
    flex: 1,
    flexDirection: 'column'
  },
  gymLogoContainer: {
    marginTop: '30rem',
    marginLeft: '30rem',
  },
  gymLogo: {
    width: '150rem',
    height: '150rem',
  },
  summaryContainer: {
    width: '92%',
    alignSelf: 'center',
    marginTop: '40 * $vUnit',
    marginBottom: '12 * $vUnit',
    justifyContent: 'center',
  },
  recordContainer: {
    width: '92%',
    height: '100 * $vUnit',
    alignSelf: 'center',
    marginBottom: '30 * $vUnit',
    justifyContent: 'center',
  },
  sectionHeader: {
    fontFamily: '$fontBold',
    fontSize: '60rem',
    color: '#000000',
    marginLeft: '40rem',
    marginBottom: '30 * $vUnit',
  },
  medalsContainer: {
    width: '100%',
    backgroundColor: '$bgColor',
    marginBottom: '30 * $vUnit'
  },
  timelineContainer: {
    width: '100%',
    marginBottom: '30 * $vUnit'
  },
  mediaContainer: {
    width: '100%',
    marginBottom: '100rem'
  },
})