import React from 'react'
import { View } from 'react-native'

import { Card, CardItem, Text, Icon } from 'native-base'

import EStyleSheet from 'react-native-extended-stylesheet'

export default function MyTimeline(props) {
  const timelineBody = props.fights.map((f, i) => {
    const timelineCardBodyContainer = i % 2 == 0 ? styles.timelineCardBodyContainerGrey : styles.timelineCardBodyContainer
    const sub = f.Sub == 1 ? <Icon type='Feather' name='check' style={styles.sub}/> : null
    return (
      <CardItem style={timelineCardBodyContainer} key={i}>
        <View style={styles.timelineCardRow}>
          <View style={styles.timelineFirstCol}>
            <Text style={styles.bodyText}>
              {f.Opponent}
            </Text>
          </View>
          <View style={styles.timelineCol}>
            <Text style={styles.bodyText}>
              {f.Win == '1' ? 'Win' : 'Loss'}
            </Text>
          </View>
          <View style={styles.timelineCol}>
            <Text style={styles.bodyTextSub}>
              {sub}
            </Text>
          </View>
          <View style={styles.timelineCol}>
            <Text style={styles.bodyText}>
              {f.Points}
            </Text>
          </View>
        </View>
      </CardItem>
    )
  })

  return (
    <View style={styles.timelineContainer}>
      <Card style={styles.timelineCardContainer}>
        <CardItem style={styles.timelineCardHeaderContainer}>
          <View style={styles.timelineCardRow}>
            <View style={styles.timelineFirstCol}>
              <Text style={styles.headerText}>
                Opponent
              </Text>
            </View>
            <View style={styles.timelineCol}>
              <Text style={styles.headerText}>
                Result
              </Text>
            </View>
            <View style={styles.timelineCol}>
              <Text style={styles.headerText}>
                Sub
              </Text>
            </View>
            <View style={styles.timelineCol}>
              <Text style={styles.headerText}>
                Match Points
              </Text>
            </View>
          </View>
        </CardItem>
        {timelineBody}
      </Card>
    </View>
	)
}

const styles = EStyleSheet.create({
  timelineContainer: {
    backgroundColor: '$bgColor',
    width: '100%',
  },
  timelineCardContainer: {
    alignSelf: 'center',
    width: '93%',
  },
  timelineCardHeaderContainer: {
    height: '175rem',
    alignSelf: 'center',
    paddingLeft: 0,
    paddingRight: 0,
  },
  timelineCardRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  timelineFirstCol: {
    width: '34%',
    justifyContent: 'center',
  },
  timelineCol: {
    justifyContent: 'center',
    width: '22%',
  },
  headerText: {
    alignSelf: 'center',
    fontFamily: '$fontSemiBold',
    fontSize: '38rem',
    color: '#3a3a3a',
  },
  timelineCardBodyContainer: {
    height: '100rem',
    alignSelf: 'center',
    paddingLeft: 0,
    paddingRight: 0,
  },
  timelineCardBodyContainerGrey: {
    height: '100rem',
    alignSelf: 'center',
    paddingLeft: 0,
    paddingRight: 0,
    backgroundColor: '#f8f8f8',
  },
  bodyText: {
    alignSelf: 'center',
    fontFamily: '$fontReg',
    fontSize: '30rem',
  },
  bodyTextSub: {
    alignSelf: 'center',
    fontFamily: '$fontReg',
    fontSize: '30rem',
  },
  sub: {
    fontSize: '50rem'
  }
})