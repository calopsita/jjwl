import React from 'react'
import { Image, ScrollView } from 'react-native'

import EStyleSheet from 'react-native-extended-stylesheet'

export default function MyMedia(props) {
  console.log('MyMedia')
  const media = props.media.map((m, i) => {
    console.log(m)
    return (
      <Image
        key={i}
        source={{uri: m}}
        style={styles.img}
        borderRadius={5}
      />
    )
  })
  console.log(media)
  return (
    <ScrollView 
      horizontal={true}
      contentContainerStyle={styles.mediaContainer}
    >
      {media}
    </ScrollView>
	)
}

const styles = EStyleSheet.create({
  mediaContainer: {
    flexGrow: 1,
  },
  img: {
    width: '400rem',
    height: '400rem',
    marginLeft: '20rem',
    marginLeft: '20rem',
  }
})