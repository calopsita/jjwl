import React from 'react'
import { Image, ScrollView } from 'react-native'

import EStyleSheet from 'react-native-extended-stylesheet'

export default function MyMedals(props) {
  const medals = props.medals.map((m, i) =>
    <Image
      key={i}
      source={{uri: m}}
      style={styles.img}
    />
  )
  return (
    <ScrollView 
      horizontal={true}
      contentContainerStyle={styles.medalContainer}
    >
      {medals}
    </ScrollView>
  )
}

const styles = EStyleSheet.create({
  medalContainer: {
    flexGrow: 1,
  },
  img: {
    width: '330rem',
    height: '330rem'
  }
})