import React from 'react'

import { Card, CardItem, Text } from 'native-base'

import EStyleSheet from 'react-native-extended-stylesheet'

export default function MySummary(props) {
  let body = props.name + ' ' + props.surname + ' is a BJJ fighter that trains at ' + props.gymName + '.'
  body == props.rankPosition == -1 ? body : body + ' Currently ranked #' + props.rankPosition + ' overall.'
  return (
    <Card style={styles.rootContainer}>
      <CardItem style={styles.headerContainer}>
        <Text style={styles.header}>
          {props.name + ' ' + props.surname}
        </Text>
      </CardItem>
      <CardItem style={styles.subheaderContainer}>
        <Text style={styles.subheader}>
          {props.gymName}
        </Text>
      </CardItem>
      <CardItem style={styles.bodyContainer}>
        <Text style={styles.body}>
          {body}
        </Text>
      </CardItem>
    </Card>
	)
}

const styles = EStyleSheet.create({
  rootContainer: {
    width: '100%',
    alignSelf: 'center',
    backgroundColor: '$bgColor',
  },
  headerContainer: {
    paddingBottom: '3 * $vUnit',
    backgroundColor: '$bgColor',
  },
  header: {
    fontFamily: '$fontSemiBold',
    fontSize: '60rem',
    color: '#3a3a3a'
  },
  subheaderContainer: {
    backgroundColor: '$bgColor',
    paddingTop: '3 * $vUnit',
    paddingBottom: '3 * $vUnit',
  },
  subheader: {
    fontFamily: '$fontMed',
    fontSize: '40rem',
    color: '#3a3a3a',
  },
  bodyContainer: {
    backgroundColor: '$bgColor',
  },
  body: {
    fontFamily: '$fontReg',
    fontSize: '30rem',
    color: '#3a3a3a'
  }
})