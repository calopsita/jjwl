import React from 'react'
import { View } from 'react-native'

import { Card, CardItem, Text } from 'native-base'

import EStyleSheet from 'react-native-extended-stylesheet'

export default function MyRecord(props) {
  return (
    <Card style={styles.rootContainer}>
      <CardItem style={styles.rowContainer}>
        <View style={styles.colContainer}>
          <View style={styles.headerContainer}>
            <Text style={styles.header}>
              Points
            </Text>
          </View>
          <View style={styles.bodyContainer}>
            <Text style={styles.body}>
              {props.points}
            </Text>
            <View style={styles.bodyAlignContainer}/>
          </View>
        </View>
        <View style={styles.dividerContainer}>
          <View style={styles.divider} />
        </View>
        <View style={styles.colContainer}>
          <View style={styles.headerContainer}>
            <Text style={styles.header}>
              Wins
            </Text>
          </View>
          <View style={styles.bodyContainer}>
            <Text style={styles.body}>
              {props.victories}
            </Text>
            <View style={styles.bodyAlignContainer}/>
          </View>
        </View>
        <View style={styles.dividerContainer}>
          <View style={styles.divider} />
        </View>
        <View style={styles.colContainer}>
          <View style={styles.headerContainer}>
            <Text style={styles.header}>
              Subs
            </Text>
          </View>
          <View style={styles.bodyContainer}>
            <Text style={styles.body}>
              {props.submissions}
            </Text>
            <View style={styles.bodyAlignContainer}/>
          </View>
        </View>
      </CardItem>
    </Card>
	)
}

const styles = EStyleSheet.create({
  rootContainer: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    marginLeft: 0,
    marginRight: 0,
  },
  rowContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
    height: '100%',
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 0,
    paddingRight: 0,
  },
  colContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  dividerContainer: {
    width: 1,
    height: '85%',
    alignSelf: 'center'
  },
  divider: {
    width: 1,
    height: '100%',
    backgroundColor: '#d3d3d3',
    justifyContent: 'center'
  },
  headerContainer: {
    justifyContent: 'center',
    alignSelf: 'center',
    width: '100%',
    height: '30%',
  },
  header: {
    fontFamily: '$fontSemiBold',
    fontSize: '40rem',
    color: '#3a3a3a',
    textAlign: 'center'
  },
  bodyContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  body: {
    fontFamily: '$fontSemiBold',
    fontSize: '100rem',
    // lineHeight: '11 * $hUnit',
    color: '#3a3a3a',
    textAlign: 'center',
    backgroundColor: 'transparent'
  },
  bodyAlignContainer: {
    height: '20%',
    backgroundColor: 'transparent'
  },
})