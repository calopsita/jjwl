import React, { Component } from 'react'

import { Provider, connect } from 'react-redux'

import createRouteNavigator from './router'

class Entry extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const signedIn = !(this.props.token == false)
    const EntryNavigator = createRouteNavigator(signedIn)
    return <EntryNavigator />
  }
}

function mapStateToProps(state) {
  return {
    token: state.token,
  }
}

export default connect(mapStateToProps)(Entry);