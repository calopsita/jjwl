import React from "react"
import { createStackNavigator, createBottomTabNavigator, createSwitchNavigator } from 'react-navigation'

import Icon from 'react-native-vector-icons/Feather'

import LoginScreen from './../login'
import FeedScreen from './../feed'
import EventsScreen from './../events'
import ProfileScreen from './../profile'
import RankingsScreen from './../rankings'
import MoreScreen from './../more'

const LoggedOut = createStackNavigator(
	{
		Login: {
			screen: LoginScreen
		},
	}
)

const LoggedIn = createBottomTabNavigator(
	{
		Feed: {
			screen: FeedScreen,
			navigationOptions: {
				tabBarIcon: ({ tintColor }) => <Icon name='list' style={{color: tintColor, fontSize: 24}}/>
			}
		},
		Events: {
			screen: EventsScreen,
			navigationOptions: {
				tabBarIcon: ({ tintColor }) => <Icon name='globe' style={{color: tintColor, fontSize: 24}}/>
			}
		},
		Profile: {
			screen: ProfileScreen,
			navigationOptions: {
				tabBarIcon: ({ tintColor }) => <Icon name='user' style={{color: tintColor, fontSize: 24}}/>
			}
		},
		Rankings: {
			screen: RankingsScreen,
			navigationOptions: {
				tabBarIcon: ({ tintColor }) => <Icon name='award' style={{color: tintColor, fontSize: 24}}/>
			}
		},
		More: {
			screen: MoreScreen,
			navigationOptions: {
				tabBarIcon: ({ tintColor }) => <Icon name='more-horizontal' style={{color: tintColor, fontSize: 24}}/>
			}
		},
	},
	{
		initialRouteName: "Rankings",
		tabBarOptions: {
			activeTintColor: '#fbfbfb',
			inactiveTintColor: '#181818',
			activeBackgroundColor: '#aeaeae',
			inactiveBackgroundColor: '#fbfbfb',
			labelStyle: {
				fontFamily: 'Montserrat-Regular'
			}
		}
	}
)

function createRouteNavigator(loggedIn) {
	console.log("ROUTE NAVIGATOR")
	console.log(loggedIn)
	console.log(LoggedIn)
	return createSwitchNavigator(
		{
			LoggedIn: {
				screen: LoggedIn
			},
			LoggedOut: {
				screen: LoggedOut
			}
		},
		{
			initialRouteName: loggedIn ? "LoggedIn": "LoggedOut"
		}
	)
}

export default createRouteNavigator