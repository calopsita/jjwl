import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { Container } from 'native-base'

import MyHeader from '../utils/header'

export default class FeedScreen extends Component {
  render() {
    return (
      <Container>
        <MyHeader title='My Feed'/>
      </Container>
    );
  }
}

const styles = StyleSheet.create({})