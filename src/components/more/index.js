import React, { Component } from 'react'
import { StyleSheet } from 'react-native'

import { connect } from 'react-redux';

import { Container, Button, Text } from 'native-base'

import MyHeader from '../utils/header'

class MoreScreen extends Component {
  constructor(props) {
    super(props)

    this.handleLogout = this.handleLogout.bind(this)
  }

  handleLogout() {
    this.props.dispatch({
      type: 'DELETE_TOKEN'
    })
  }

  render() {
    return (
      <Container>
        <MyHeader title='More'/>
        <Container style={styles.bodyContainer}>
          <Button dark block onPress={this.handleLogout}>
          	<Text>Logout</Text>
          </Button>
        </Container>
      </Container>
    );
  }
}

export default connect()(MoreScreen)

const styles = StyleSheet.create({
  bodyContainer: {
    flex: 1,
    justifyContent: 'center'
  }
})