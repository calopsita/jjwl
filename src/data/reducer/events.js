import { createReducer } from './utils'

const ddEvents = [
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    date: '2018-03-13',
    title: 'Las Vegas Tournament 2018',
    arena: 'Las Vegas Arena',
    subtitle: 'Price Increases 2/12',
    ages: 'Youth, Adults & Masters',
    styles: 'Gi & No Gi',
    registered: true
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    date: '2018-03-30',
    title: 'Candyland Tournament 2018',
    arena: 'Candyland Arena',
    subtitle: 'Price Increases 3/30',
    ages: 'Youth, Adults & Masters',
    styles: 'Gi & No Gi',
    registered: false
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    date: '2018-04-13',
    title: 'Las Vegas Tournament 2018',
    arena: 'Las Vegas Arena',
    subtitle: 'Price Increases 3/12',
    ages: 'Youth, Adults & Masters',
    styles: 'Gi & No Gi',
    registered: true
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    date: '2018-04-30',
    title: 'Las Vegas Tournament 2018',
    arena: 'Las Vegas Arena',
    subtitle: 'Price Increases 3/30',
    ages: 'Youth, Adults & Masters',
    styles: 'Gi & No Gi',
    registered: true
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    date: '2019-03-13',
    title: 'Boston Tournament 2019',
    arena: 'Boston Arena',
    subtitle: 'Price Increases 2/12',
    ages: 'Youth, Adults & Masters',
    styles: 'Gi & No Gi',
    registered: true
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    date: '2019-03-30',
    title: 'Candyland Tournament 2019',
    arena: 'Candyland Arena',
    subtitle: 'Price Increases 3/30',
    ages: 'Youth, Adults & Masters',
    styles: 'Gi & No Gi',
    registered: false
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    date: '2019-04-13',
    title: 'Los Angeles Tournament 2019',
    arena: 'Los Angeles Arena',
    subtitle: 'Price Increases 3/12',
    ages: 'Youth, Adults & Masters',
    styles: 'Gi & No Gi',
    registered: true
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    date: '2019-04-30',
    title: 'Denver Tournament 2019',
    arena: 'Denver Arena',
    subtitle: 'Price Increases 3/30',
    ages: 'Youth, Adults & Masters',
    styles: 'Gi & No Gi',
    registered: true
  }
]

function retrieve(events, action) {
	const newEvents = ddEvents
  return newEvents
}

const events = createReducer([], {
	RETRIEVE_EVENTS: retrieve
})

export default events