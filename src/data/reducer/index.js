import { combineReducers } from 'redux'

import rankings from './rankings'
import profile from './profile'
import events from './events'
import token from './token'
import rankFilter from'./rank_filter'
import rankFilterCats from './rank_filter_cats'

const reduce = combineReducers({
  rankings,
  rankFilter,
  rankFilterCats,
  profile,
  events,
  token,
})

export { reduce }