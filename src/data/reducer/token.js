import { createReducer } from './utils'

function retrieve(tok, action) {
	const newTok = action.data.msg
  return newTok
}

function del() {
	const newTok = undefined
	return newTok
}

const token = createReducer(false, {
	RETRIEVE_TOKEN_SUCCEEDED: retrieve,
	DELETE_TOKEN: del,
})

export default token