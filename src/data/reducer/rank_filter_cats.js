import produce from 'immer'
import assign from 'assign-deep'

import { createReducer } from './utils'

const initAgeGroup = () => {
  return ({
    0: { id: 0, n: 'Adult' },
    1: { id: 1, n: 'Youth' },
  })
}

const initGender = () => {
  return ({
    0: { id: 0, n: 'Male' },
    1: { id: 1, n: 'Female' },
  })
}

const initGroupDetails = () => {
  const ageGroup = initAgeGroup()
  const gender = initGender()
  return ({
    1: {ageGroup: ageGroup['1'], gender: gender['0']},
    2: {ageGroup: ageGroup['1'], gender: gender['1']},
    3: {ageGroup: ageGroup['0'], gender: gender['1']},
    4: {ageGroup: ageGroup['0'], gender: gender['0']},
  })
}

const initSeason = () => {
  return ({
    0: { id: 0, n: '2016' },
    1: { id: 1, n: '2017' },
    2: { id: 2, n: '2018' },
  })
}

const initCategoryData = () => {
  return ({
    season: initSeason(),
    ageGroup: initAgeGroup(),
    gender: initGender(),
    g: initGroupDetails(),
    a: {},
    b: {},
    w: {},
  })
}

const initCombination = () => {
  return ({
    g: null,
    a: null,
    b: null,
    w: null,
  })
}

const initCategoryIds = () => {
  return ({
    a: new Set(),
    b: new Set(),
    w: new Set(),
  })
}

const initRankFilterCats = (overrides) => {
  const defaults = {
    combinationIds: [],
    categoryData: initCategoryData() 
  }
  const out = assign(defaults, overrides)
  return out
}

const makeGetComboIds = () => {
  const cats = ['g', 'a', 'b', 'w']
  const numCats = cats.length
  return getComboIds = (v) => {
    let ok = true
    const comboIds = initCombination()
    if (Number(v.g.id) > 4) {
      ok = false
    } else {
      for (let i = 0; i < numCats; i++) {
        const cat = cats[i]
        const catId = v[cat].id
        if (catId == '0') { 
          ok = false
          break
        } else { 
          comboIds[cat] = catId
        }
      }
    }
    if (ok) {
      return comboIds
    } else {
      return null
    }
  }
}

const stringifyComboIds = (comboIds) => ('g' + comboIds.g + 'a' + comboIds.a + 'b' + comboIds.b + 'w' + comboIds.w)

const retrieve = produce((draft, action) => {
  const getComboIds = makeGetComboIds()
  const updateCats = ['a', 'b', 'w']
  const numUpdateCats = updateCats.length
  let combinationIdsStrs = new Set()
  let combinationIds = []
  let categoryIds = initCategoryIds()
  let categoryData = initCategoryData()
  for (let [k, v] of Object.entries(action.data.msg)) {
    const comboIds = getComboIds(v)
    if (comboIds != null) {
      const comboIdsStr = stringifyComboIds(comboIds)
      const combinationIdsStrsSize = combinationIdsStrs.size
      combinationIdsStrs.add(comboIdsStr)
      if (combinationIdsStrsSize < combinationIdsStrs.size) {
        combinationIds.push(comboIds)
        for (let i = 0; i < numUpdateCats; i++) {
          const cat = updateCats[i]
          const catData = v[cat]
          const catId = catData.id
          const catSize = categoryIds[cat].size
          categoryIds[cat].add(catId)
          if (catSize < categoryIds[cat].size) {
            categoryData[cat][catId] = {...catData, n: catData.n.trim()}
          }
        }
      }
    }
  }
  assign(draft, { combinationIds: combinationIds, categoryData: categoryData })
})

const rankFilterCats = createReducer(initRankFilterCats(), {
  RETRIEVE_RANK_FILTER_CATS_SUCCEEDED: retrieve,
})

export default rankFilterCats