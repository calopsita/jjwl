import produce from 'immer'
import assign from 'assign-deep'

import { createReducer } from './utils'

const updateModal = produce((draft, action) => {
  draft.modal = action.cat
})

const updateMultiCheck = produce((draft, action) => {
  const toUpdate = draft.pending[action.cat]
  const next = new Set(toUpdate)
  if (next.has(action.id)) {
    next.delete(action.id)
  } else {
    next.add(action.id)
  }
  assign(draft, { pending: { [action.cat]: next } })
})

const updateGroup = produce((draft, action) => {
  assign(draft, { pending: { g: action.id } })
})

const initRankFilter = () => {
  const initFields = () => ({
    g: '1',
    a: new Set(),
    b: new Set(),
    w: new Set(),
    season: new Set(),
  })
  return ({
    modal: null,
    pending: initFields(),
    active: initFields(),
  })
}

const rankFilter = createReducer(initRankFilter(), {
  UPDATE_RANK_FILTER_MODAL: updateModal,
	UPDATE_RANK_FILTER_MULTICHECK: updateMultiCheck,
  UPDATE_RANK_FILTER_GROUP: updateGroup,
})

export default rankFilter