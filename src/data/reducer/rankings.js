import { createReducer } from './utils'

const ddRankings = [
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    name: 'Nikola Jokic',
    gymName: 'Denver Nuggets',
    timelines: {
      '2018': [
        {
          name: 'San Diego Super Championship',
          medal: {rank: 1, img: 'https://www.jjworldleague.com/uploaded/medals/82_goldmedal.jpg'},
          timeline: [
            {
              opponent: 'Charles Tinley',
              result: 'submission',
              rankingPoints: 5,
            },
            {
              opponent: 'Bongo Tinley',
              result: 'win',
              rankingPoints: 3,
            },
            {
              opponent: 'Bumper Tinley',
              result: 'loss',
              rankingPoints: 0,
            },
          ]
        },
        {
          name: 'Los Angeles Super Championship',
          medal: {rank: 2, img: 'https://www.jjworldleague.com/uploaded/medals/86_silvermedal.jpg'},
          timeline: [
            {
              opponent: 'Charles Tinley',
              result: 'submission',
              rankingPoints: 5,
            },
            {
              opponent: 'Bongo Tinley',
              result: 'win',
              rankingPoints: 3,
            },
            {
              opponent: 'Bumper Tinley',
              result: 'loss',
              rankingPoints: 0,
            },
          ]
        },
      ],
      '2017': [
        {
          name: 'San Diego Super Championship',
          medal: {rank: 1, img: 'https://www.jjworldleague.com/uploaded/medals/82_goldmedal.jpg'},
          timeline: [
            {
              opponent: 'Charles Tinley',
              result: 'submission',
              rankingPoints: 5,
            },
            {
              opponent: 'Bongo Tinley',
              result: 'win',
              rankingPoints: 3,
            },
            {
              opponent: 'Bumper Tinley',
              result: 'loss',
              rankingPoints: 0,
            },
          ]
        },
        {
          name: 'Los Angeles Super Championship',
          medal: {rank: null, img: null},
          timeline: [
            {
              opponent: 'Charles Tinley',
              result: 'submission',
              rankingPoints: 5,
            },
            {
              opponent: 'Bongo Tinley',
              result: 'win',
              rankingPoints: 3,
            },
            {
              opponent: 'Bumper Tinley',
              result: 'loss',
              rankingPoints: 0,
            },
          ]
        },
      ],
    },
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    name: 'Jamal Murray',
    gymName: 'Denver Nuggets',
    rankPoints: 90,
    medalCount: {1: 2, 2: 3, 3: 4},
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    name: 'Paul Milsap',
    gymName: 'Denver Nuggets',
    rankPoints: 90,
    medalCount: {1: 2, 2: 3, 3: 4},
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    name: 'Gary Harris',
    gymName: 'Denver Nuggets',
    rankPoints: 80,
    medalCount: {1: 2, 2: 3, 3: 4},
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    name: 'Mason Plumlee',
    gymName: 'Denver Nuggets',
    rankPoints: 60,
    medalCount: {1: 2, 2: 3, 3: 4},
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    name: 'Juancho Hernangomez',
    gymName: 'Denver Nuggets',
    rankPoints: 50,
    medalCount: {1: 2, 2: 3, 3: 4},
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    name: 'Trey Lyles',
    gymName: 'Denver Nuggets',
    rankPoints: 40,
    medalCount: {1: 2, 2: 3, 3: 4},
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    name: 'Monte Morris',
    gymName: 'Denver Nuggets',
    rankPoints: 30,
    medalCount: {1: 2, 2: 3, 3: 4},
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    name: 'Malik Beasley',
    gymName: 'Denver Nuggets',
    rankPoints: 30,
    medalCount: {1: 2, 2: 3, 3: 4},
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    name: 'Will Barton',
    gymName: 'Denver Nuggets',
    rankPoints: 20,
    medalCount: {1: 2, 2: 3, 3: 4},
  },
  {
    img: 'https://www.jjworldleague.com/img/default_avatar.png',
    name: 'Isaiah Thomas',
    gymName: 'Denver Nuggets',
    rankPoints: 10,
    medalCount: {1: 2, 2: 3, 3: 4},
  }
]


function retrieve(rankings, action) {
  const newRankings = ddRankings
  return newRankings
}

const rankings = createReducer([], {
	RETRIEVE_RANKINGS: retrieve
})

export default rankings