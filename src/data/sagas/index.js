import { call, put, all, takeLatest } from 'redux-saga/effects'

import API from '../api'

function *apiPost(action) {
  console.log("API POST")
  console.log(action)
  try {
    const response = yield call(API.post, action.task, action.payload)
    console.log('********')
    console.log(response)
    let data
    try {
      data = response ? {data: JSON.parse(response.data.message)} : {}
    } catch (error) {
      data = response ? {data: {msg: response.data.message}} : {}
    }
    data._passthrough = action.passthrough != undefined ? action.passthrough : {}
    const newAction = Object.assign({}, {type: action.nextTypeSuccess}, data)
    console.log('NEW DATA')
    console.log(data)
    console.log('NEW ACTION')
    console.log(newAction)
    yield put(newAction)
  } catch (error) {
    console.log('ERROR')
    console.log(error)
    console.log(error.response.data)
  }
}


function *rootSaga() {
  yield all([
    takeLatest('POST', apiPost),
  ])
}

export default rootSaga;