import axios from 'axios'
import qs from 'qs';

export default class API {

  static post(task, payload) {
    const url = 'https://www.jjworldleague.com/dynamic/api/api_app.php'
    const body = Object.assign({}, {type: task}, payload)
    const bodyStr = qs.stringify(body)
    return (
      axios.post(url, bodyStr)
    )
  }
}