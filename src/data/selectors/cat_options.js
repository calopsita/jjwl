import produce from 'immer'
import assign from 'assign-deep'

import { createSelector } from 'reselect'

const initUniqueIds = () => {
  return ({
    a: new Set(),
    w: new Set(),
    b: new Set(), 
  })
}

// const uniqueIds = Immutable.Record({
//   a: Immutable.Set(),
//   w: Immutable.Set(),
//   b: Immutable.Set(),
// })

const initCatOptions = () => {
  return ({
    a: [],
    w: [],
    b: [],
  })
}

// const uniqueData = Immutable.Record({
//   a: Immutable.Set(),
//   w: Immutable.Set(),
//   b: Immutable.Set(),
// })

// const uniqueEntries = Immutable.Record({
//   ids: uniqueIds(),
//   data: uniqueData(),
// })

// TODO: once names are cleaned, don't need the id check
// TODO: only youth female has green, no entries for youth male

const getCatOptions = createSelector(
  (state) => state.rankFilter.pending,
  (state) => state.rankFilterCats,
  (pending, rankFilterCats) => {
    const multiCats = ['a', 'b', 'w']
    const filterCriteria = multiCats.reduce((obj, i, idx, arr) => {
      const otherCats = [...arr.slice(0, idx), ...arr.slice(idx + 1)]
      const catFilterCriteria = otherCats.reduce((obj, i) => { 
        if (pending[i].size > 0) { obj[i] = pending[i] } ; return obj
      }, {})
      obj[i] = catFilterCriteria
      return obj
    }, {})
    const uniqueIds = initUniqueIds()
    const catOptions = rankFilterCats.combinationIds.reduce((obj, i) => {
      if (i.g == pending.g) {
        for (let cat of multiCats) {
          let ok = true
          for (let [k, v] of Object.entries(filterCriteria[cat])) {
            if (!v.has(i[k])) {
              ok = false
              break
            }
          }
          if (ok) {
            const uniqueIdsSize = uniqueIds[cat].size
            uniqueIds[cat].add(i[cat])
            if (uniqueIdsSize < uniqueIds[cat].size) {
              obj[cat].push(rankFilterCats.categoryData[cat][i[cat]])
            }
          }
        }
      }
      return obj
    }, initCatOptions())
    return catOptions
  }
)

// const getCatOptions = createSelector(
//   (state) => state.rankFilter.pending,
//   (state) => state.rankFilterCats,
//   (pending, rankFilterCats) => {
//     let test = Immutable.Set()
//     const multiCats = Immutable.Set(['a', 'b', 'w'])
//     const activeStatus = Immutable.Map(multiCats.map(v => (
//       [v, Immutable.Map({all: pending.get(v).isEmpty(), selected: pending.get(v)})])))
//     const filterCriteria = Immutable.Map(multiCats.map(v => (
//       [v, activeStatus.delete(v)])))    
//     const catOptions = uniqueEntries().withMutations((ue) => {
//       rankFilterCats.combinationIds.forEach(i => {
//         if (i.get('w') == "49") {
//           test = test.add(i)
//         }
//         if (i.get('g') == pending.g) {
//           for (let cat of multiCats) {
//             const fc = filterCriteria.get(cat)
//             const ok = true
//             for (let [k, v] of fc) {
//               if (!(v.get('all') || v.get('selected').has(i.get(k)))) {
//                 ok = false
//                 break
//               }
//             }
//             if (ok) {
//               if (!ue.ids.get(cat).has(i)) {
//                 ue.updateIn(['ids', cat], ii => ii.add(i.get(cat)))
//                 ue.updateIn(['data', cat], ii => ii.add(rankFilterCats.categoryData.getIn([cat, i.get(cat)])))
//               }
//             }
//           }
//         }
//       })
//     })
//     return catOptions.data
//   }
// )

export default getCatOptions