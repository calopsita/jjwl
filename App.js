import React, { Component } from 'react'

import { Provider, connect } from 'react-redux'

import createRouteNavigator from './router'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    store.dispatch({type: 'RETRIEVE_SAVED_TOKEN'})
  }

  render() {
    console.log("KKKKK")
    console.log(this.props)
    if (this.props.token == undefined) {
      return null
    } else {
      const signedIn = this.props.token != null
      const Navigator = createRouteNavigator(signedIn)
      return (
        <Navigator />
      )
    }
  }
}

function mapStateToProps(state) {
  console.log("YYYYYY")
  console.log(state)
  return {
    token: state.token,
  }
}

export default connect(mapStateToProps)(App);